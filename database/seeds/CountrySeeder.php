<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//factory(App\Countries::class)->make();

         DB::table('country')->insert([
            'country' => 'Nigeria',
            'dial_code' => '+234',
            'active' => '1',
        ]);

        DB::table('country')->insert([
            'country' => 'Ghana',
            'dial_code' => '+233',
            'active' => '0',
        ]);

    }
}
