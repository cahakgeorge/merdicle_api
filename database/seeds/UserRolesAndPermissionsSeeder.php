<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserRolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // Reset cached roles and permissions
         app()['cache']->forget('spatie.permission.cache');
        
         //create all permissions
         $this->createAllPermissions();
 
         // create roles and assign created permissions
         $this->createTestAdminAndPermissions();
         $this->createDoctorAndPermissions();
         $this->createPatientAndPermissions();
    }
    
    //create all permissions
    public function createAllPermissions(){

        // create permissions
        Permission::create(['name' => 'view dashboard']);
        Permission::create(['name' => 'view user detail']);
        Permission::create(['name' => 'update user detail']);
        Permission::create(['name' => 'update user status']);
        Permission::create(['name' => 'search for users']);
    }

    public function createTestAdminAndPermissions(){
        $role = Role::create(['name' => 'super-admin']);

        $role->givePermissionTo(['view dashboard', 'view user detail', 'update user detail',
                'update user status', 'search for users' ]);
    }

    public function createDoctorAndPermissions(){
        $role = Role::create(['name' => 'doctor']);
    }

    public function createPatientAndPermissions(){
        $role = Role::create(['name' => 'patient']);
    }

}
