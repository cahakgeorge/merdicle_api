<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

use Carbon\Carbon;
use App\Helpers\NetworkHelper;

class CreateMerchantBankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banks = $this->getBanksFromApi();
        DB::table('merchant_bank')->insert($banks);
    }
    
    public function getBanksFromApi(){
        $returnableBankArray = []; //setup and array to hold list of banks in format we want
        $response = $this->makeNetworkCallToPaystackApi(); //call function to make network call to paystack API
        
        if(!empty($response) && is_array($response) && $response[0]==200){ //sort through the response data if available
            $response = json_decode($response[1]);//decode json response

            $responseData = $response->data; //data field contains bank list

            for($i=0; $i<sizeof($responseData); $i++){
                
                $returnableBankArray[] = ['name'=>$responseData[$i]->name, 'type'=>'1', 
                                        'code'=>$responseData[$i]->code, 
                                        'ibank_link'=>$this->resolveIbBank($responseData[$i]->code)];
            }

            return $returnableBankArray;
        }
    }

    public function makeNetworkCallToPaystackApi(){
        $apiUrl = "https://api.paystack.co/bank";
        $networkHelper =  new NetworkHelper();

        return $networkHelper->makeRequestCurl($apiUrl);
    }

    public function resolveIbBank($bankCode){
        $iBankLinks = $this->getBankLinkArray();

        if(array_key_exists($bankCode, $iBankLinks)) return $iBankLinks[$bankCode];

        return 'https://ibank.accessbankplc.com/RetailBank/';
    }

    public function getBankLinkArray(): array{
        return ['044'=>'https://ibank.accessbankplc.com/RetailBank/#/', '035A'=>'http://www.wemabank.com/internet-banking/', '401'=>'http://www.asoplc.com/products/web/', '023'=>'', 
                '063'=>'https://diamondonline.diamondbank.com/web/guest', '050'=>'https://www.ecobank.com/personal-banking/security-centre/banking/internet-banking', '562'=>'', '084'=>'',
                
                '070'=>'https://online.fidelitybank.ng/#/login', '011'=>'https://www.firstbanknigeria.com/personal-banking/ways-to-bank/online-banking/',
                '214'=>'https://ibank.fcmb.com/Individual-Banking.aspx', '058'=>'https://ibank.gtbank.com/ibank3/Alert.aspx', 
                '030'=>'https://ebank.hbng.com/hibibanking/HomePages/FirstAccessDoorUI', '301'=>'', '082'=>'https://ibank.keystonebankng.com/ibank/',
            
                '014'=>'', '526'=>'', '076'=>'https://www.polarisbanklimited.com/polarisxperience/', '101'=>'', '221'=>'https://ibanking.stanbicibtcbank.com/Login',
                '068'=>'https://online.standardchartered.com/nfsafr/ibank/ng/foa/login.htm', '232'=>'https://bankonline.sterlingbankng.com/web/guest',
                '100'=>'https://www.suntrustng.com/internet-banking/', '032'=>'https://uniononline.unionbankng.com/OnlineBanking/',
                '033'=>'https://ibank.ubagroup.com', '215'=>'https://www.unitybankng.com/products/single/internet-banking',
                '035'=>'http://www.wemabank.com/internet-banking/', '057'=>'https://ibank.zenithbank.com/InternetBanking/App/Security/login'];

                
    }
}
