<?php

use Illuminate\Database\Seeder;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* factory(App\Models\State::class, 36)->create()->each(function($u) {
		    //do something else, such as create user
		}); */
        DB::table('state')->insert([
            ['state' => 'Abia', 'country_id' => '1', 'country' => 'Nigeria',], 
            [ 'state' => 'Adamawa', 'country_id' => '1', 'country' => 'Nigeria',],
            ['state' => 'Akwa Ibom', 'country_id' => '1', 'country' => 'Nigeria',], 
            [ 'state' => 'Anambra', 'country_id' => '1', 'country' => 'Nigeria',],
            ['state' => 'Bauchi', 'country_id' => '1', 'country' => 'Nigeria',], 
            [ 'state' => 'Bayelsa', 'country_id' => '1', 'country' => 'Nigeria',],
            ['state' => 'Benue', 'country_id' => '1', 'country' => 'Nigeria',], 
            [ 'state' => 'Borno', 'country_id' => '1', 'country' => 'Nigeria',],
            ['state' => 'Cross River', 'country_id' => '1', 'country' => 'Nigeria',], 
            [ 'state' => 'Delta', 'country_id' => '1', 'country' => 'Nigeria',],
            ['state' => 'Ebonyi', 'country_id' => '1', 'country' => 'Nigeria',], 
            [ 'state' => 'Edo', 'country_id' => '1', 'country' => 'Nigeria',],
            [ 'state' => 'Ekiti', 'country_id' => '1', 'country' => 'Nigeria',],
            ['state' => 'Enugu', 'country_id' => '1', 'country' => 'Nigeria',], 
            [ 'state' => 'FCT', 'country_id' => '1', 'country' => 'Nigeria',],
            [ 'state' => 'Gombe', 'country_id' => '1', 'country' => 'Nigeria',],
            ['state' => 'Imo', 'country_id' => '1', 'country' => 'Nigeria',], 
            [ 'state' => 'Jigawa', 'country_id' => '1', 'country' => 'Nigeria',],
            ['state' => 'Kaduna', 'country_id' => '1', 'country' => 'Nigeria',], 
            [ 'state' => 'Kano', 'country_id' => '1', 'country' => 'Nigeria',],
            [ 'state' => 'Katsina', 'country_id' => '1', 'country' => 'Nigeria',],
            ['state' => 'Kebbi', 'country_id' => '1', 'country' => 'Nigeria',], 
            [ 'state' => 'Kogi', 'country_id' => '1', 'country' => 'Nigeria',],
            ['state' => 'Kwara', 'country_id' => '1', 'country' => 'Nigeria',], 
            [ 'state' => 'Lagos', 'country_id' => '1', 'country' => 'Nigeria',],
            ['state' => 'Nasarawa', 'country_id' => '1', 'country' => 'Nigeria',], 
            [ 'state' => 'Niger', 'country_id' => '1', 'country' => 'Nigeria',],
            [ 'state' => 'Ogun', 'country_id' => '1', 'country' => 'Nigeria',],
            ['state' => 'Ondo', 'country_id' => '1', 'country' => 'Nigeria',], 
            [ 'state' => 'Osun', 'country_id' => '1', 'country' => 'Nigeria',],
            ['state' => 'Oyo', 'country_id' => '1', 'country' => 'Nigeria',], 
            [ 'state' => 'Plateau', 'country_id' => '1', 'country' => 'Nigeria',],
            ['state' => 'Rivers', 'country_id' => '1', 'country' => 'Nigeria',], 
            [ 'state' => 'Sokoto', 'country_id' => '1', 'country' => 'Nigeria',],
            ['state' => 'Taraba', 'country_id' => '1', 'country' => 'Nigeria',], 
            [ 'state' => 'Yobe', 'country_id' => '1', 'country' => 'Nigeria',],
            [ 'state' => 'Zamfara', 'country_id' => '1', 'country' => 'Nigeria',],
        ]);

    }
}
