<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

use Carbon\Carbon;

use App\Models\Admin;

class CreateAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Admin::create([
            'name' => 'Cahak George',
            'email' => 'cahakgeorge@gmail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
            'status' => '1',
            'created_at' => now(),
        ]);

        $admin->assignRole('super-admin'); 
        
    }
}
