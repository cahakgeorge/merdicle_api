<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForgotPasswordTokenTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'forgot_password_token';

    /**
     * Run the migrations.
     * @table forgot_password_token
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('search_input', 191)->comment('email address or phone number');
            $table->string('type', 5)->comment('email or phone');
            $table->string('token', 100);
            $table->string('active', 5);
            $table->timestamp('created_at');

            $table->index(["user_id"], 'fk_fpassword_user_idx');


            $table->foreign('user_id', 'fk_fpassword_user_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
