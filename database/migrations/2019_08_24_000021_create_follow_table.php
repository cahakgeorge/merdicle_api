<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'follow';

    /**
     * Run the migrations.
     * @table follow
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('This person is a follower of the “user” in “follows_user_id”');
            $table->unsignedInteger('follows_user_id')->comment('id of user being followed');
            $table->dateTime('created_at');

            $table->index(["follows_user_id"], 'follower');

            $table->index(["user_id"], 'initiator');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
