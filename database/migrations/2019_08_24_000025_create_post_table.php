<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'post';

    /**
     * Run the migrations.
     * @table post
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('Id of user who posted this');
            $table->string('post_type', 10)->comment('normal, story, health, journal');
            $table->string('visibility', 15)->nullable()->comment('all, contacts, followers, friend of contacts/followers');
            $table->string('has_extra', 5)->comment('nil, img, imgs, vid, doc');
            $table->string('content')->nullable()->comment('content of the post');
            $table->string('post_is_for', 11)->nullable()->comment('comment/share/case_review/hosp_review/hub/assoc');
            $table->string('parent_post_is_for', 11)->nullable()->comment('shows who original post belongs to: only interested in whether it was for assoc or hub though');
            $table->integer('owner_id')->nullable()->comment(' id of original post/hospital/case/hub/assoc who owns this');
            $table->string('extra_url')->nullable();

            $table->index(["user_id"], 'fk_post_user_idx');
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('user_id', 'fk_post_user_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
