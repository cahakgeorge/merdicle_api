<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHubMemberTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'hub_member';

    /**
     * Run the migrations.
     * @table hub_member
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('hub_id');
            $table->increments('user_id');
            $table->dateTime('created_at');
            $table->tinyInteger('status')->nullable()->comment('approved/pending/blocked');

            $table->index(["user_id"], 'users_id');

            $table->index(["hub_id"], 'hub_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
