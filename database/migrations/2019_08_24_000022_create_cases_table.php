<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'cases';

    /**
     * Run the migrations.
     * @table cases
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 100);
            $table->unsignedInteger('creator_id');
            $table->longText('case_description');
            $table->longText('symptoms')->nullable();
            $table->longText('diagnosis')->nullable();
            $table->longText('complications')->nullable();
            $table->longText('causes')->nullable();
            $table->longText('treatment')->nullable();
            $table->string('has_extra', 3)->comment('nil, pic, vid, doc');
            $table->string('extra_url')->nullable();

            $table->index(["creator_id"], 'creator_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
