<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'appointment';

    /**
     * Run the migrations.
     * @table appointment
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('doctor_id');
            $table->string('title', 100);
            $table->string('description', 250)->nullable();
            $table->date('date');
            $table->string('location', 150)->nullable()->comment('Address appointment is happening at');
            $table->string('status', 10)->nullable()->default('pending')->comment('pending/accepted/cancelled');
            $table->string('edit_log')->nullable();

            $table->index(["user_id"], 'fk_appointment_user_id_idx');

            $table->index(["doctor_id"], 'fk_appointment_doctor_id_idx');
            $table->timestamps();


            $table->foreign('user_id', 'fk_appointment_user_id_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('doctor_id', 'fk_appointment_doctor_id_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
