<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaseFileTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'case_file';

    /**
     * Run the migrations.
     * @table case_file
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('file_path', 100);
            $table->string('file_type', 10);
            $table->unsignedInteger('case_id');

            $table->index(["case_id"], 'case_id');
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('case_id', 'case_id')
                ->references('id')->on('cases')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
