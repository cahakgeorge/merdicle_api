<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStateTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'state';

    /**
     * Run the migrations.
     * @table state
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('state', 100);
            $table->string('country', 100);
            $table->unsignedInteger('country_id');
            $table->dateTime('created_at')->nullable();

            $table->index(["country_id"], 'fk_state_country_idx');


            $table->foreign('country_id', 'fk_state_country_idx')
                ->references('id')->on('country')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
