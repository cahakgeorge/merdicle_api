<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'notification';

    /**
     * Run the migrations.
     * @table notification
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('for_who')->comment('Id of the target user');
            $table->string('type', 10)->comment('follow, like, share, mention, hub, assoc');
            $table->string('message', 200)->comment('notification message with placeholders such as user/assoc/hub names, to be completed later in app');
            $table->string('placeholder_values', 100)->comment('user names available in the notification message');
            $table->string('pic_url', 100)->nullable();

            $table->index(["for_who"], 'user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
