<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhoneBookTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'phone_book';

    /**
     * Run the migrations.
     * @table phone_book
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('uploaded_by')->comment('Id of user who uploaded contact');
            $table->string('contact_phone', 14)->comment('contact number');
            $table->string('contact_email', 191)->comment('contact number');
            $table->string('contact_name', 100)->comment('name of contact if available');
            $table->tinyInteger('found')->default('0')->comment('is user on platform or not');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->index(["uploaded_by"], 'fk_pbook_uploader_idx');


            $table->foreign('uploaded_by', 'fk_pbook_uploader_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
