<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePracticeTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'practice';

    /**
     * Run the migrations.
     * @table practice
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('name', 191);
            $table->string('type', 40);
            $table->dateTime('start_date')->nullable()->comment('');
            $table->string('job_desc')->nullable();
            $table->string('country', 100)->nullable();

            $table->index(["user_id"], 'fk_practice_user_idx');
            $table->timestamps();


            $table->foreign('user_id', 'fk_practice_user_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
