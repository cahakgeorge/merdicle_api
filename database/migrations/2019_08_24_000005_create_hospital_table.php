<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'hospital';

    /**
     * Run the migrations.
     * @table hospital
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 100);
            $table->string('owner', 100);
            $table->string('address', 120);
            $table->string('email', 100)->nullable();
            $table->string('phone', 15)->nullable();
            $table->unsignedSmallInteger('staff_strength')->nullable();
            $table->string('bio', 200)->nullable();
            $table->string('latlng', 50)->nullable()->comment('hospital coordinates');
            $table->tinyInteger('consultant_count')->nullable()->comment('Number of consultants in hospital');
            $table->tinyInteger('specialist_count')->nullable()->comment('Number of specialists in hospital');
            $table->string('type', 20)->nullable()->comment('hospital type - hospital/clinic...');
            $table->string('recieve_appoints', 5)->comment('whether the hospital is open to recieve appointments from merdTrue or false');
            $table->string('services', 200)->nullable();
            $table->string('pic_url', 100)->nullable()->default(null);
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
