<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferralTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'referral';

    /**
     * Run the migrations.
     * @table referral
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->comment('Bid Id
');
            $table->unsignedInteger('referer_id')->comment('Id of user who referred others');
            $table->unsignedInteger('referee_id')->comment('Id of user who was referred to plaform');
            $table->string('type', 10)->comment('referral type: via referal code/social media post');
            $table->string('platform', 10)->nullable()->comment('platform used in making referral');
            $table->dateTime('created_at');

            $table->index(["referee_id"], 'fk_referee_user_idx');

            $table->index(["referer_id"], 'fk_referrer_user_idx');


            $table->foreign('referee_id', 'fk_referee_user_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('referer_id', 'fk_referrer_user_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
