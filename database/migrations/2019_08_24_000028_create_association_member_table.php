<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssociationMemberTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'association_member';

    /**
     * Run the migrations.
     * @table association_member
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('association_id');
            $table->unsignedInteger('user_id');
            $table->dateTime('created_at');
            $table->string('status', 8)->nullable()->comment('open/approved/pending/blocked');

            $table->index(["user_id"], 'fk_amember_user_idx');


            $table->foreign('user_id', 'fk_amember_user_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
