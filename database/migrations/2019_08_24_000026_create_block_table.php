<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'block';

    /**
     * Run the migrations.
     * @table block
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('This user blocked the user in blocked_id');
            $table->unsignedInteger('blocked_id')->comment('id of user that was blocked');
            $table->dateTime('created_at');
            $table->string('report', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
