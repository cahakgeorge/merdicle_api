<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalRatingTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'hospital_rating';

    /**
     * Run the migrations.
     * @table hospital_rating
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('hospital_id');
            $table->unsignedInteger('user_id');
            $table->decimal('rating', 2, 1)->comment('rating shouldnt be more than 5');
            $table->dateTime('delete_at')->nullable();

            $table->index(["user_id"], 'fk_hrating_user_idx');

            $table->index(["hospital_id"], 'fk_hrating_hospital_idx');
            $table->timestamps();


            $table->foreign('hospital_id', 'fk_hrating_hospital_idx')
                ->references('id')->on('hospital')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('user_id', 'fk_hrating_user_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
