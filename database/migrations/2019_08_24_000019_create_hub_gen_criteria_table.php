<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHubGenCriteriaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'hub_gen_criteria';

    /**
     * Run the migrations.
     * @table hub_gen_criteria
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('hub_id');
            $table->string('criteria_text', 200)->comment('text describing criteria used in forming hub');
            $table->integer('criteria_key')->comment('corresponding foreign key if available');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->index(["hub_id"], 'fk_gencriteria_hub_idx');


            $table->foreign('hub_id', 'fk_gencriteria_hub_idx')
                ->references('id')->on('hub')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
