<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'feedback';

    /**
     * Run the migrations.
     * @table feedback
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('user id who left feedback');
            $table->longText('details')->comment('details of the product');
            $table->unsignedTinyInteger('has_attachment')->comment('admin who created this');
            $table->unsignedTinyInteger('type')->comment('type of feedback: product/winner/others');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('When was product entry made?');

            $table->index(["user_id"], 'fk_feedback_user_idx');


            $table->foreign('user_id', 'fk_feedback_user_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
