<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssociationTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'association';

    /**
     * Run the migrations.
     * @table association
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 50);
            $table->string('admins', 100);
            $table->longText('bio')->nullable()->default(null);
            $table->string('pic_url', 100);
            $table->string('free_entry', 5)->comment('is association an open one? do users need to be approved to join');
            $table->unsignedInteger('created_by');

            $table->index(["created_by"], 'fk_associated_created_by_user_idx');
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('created_by', 'fk_associated_created_by_user_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
