<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'user';

    /**
     * Run the migrations.
     * @table user
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('email')->nullable();
            $table->string('phone', 11)->nullable();
            $table->string('dial_code', 4)->nullable();
            $table->string('first_name', 100);
            $table->string('last_name', 100);
            $table->string('password', 100);
            $table->string('bio');
            $table->string('user_type', 8);
            $table->string('user_pic_url', 100)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('status', 9)->comment('new/active/blocked/suspended');
            $table->string('remember_token', 80)->nullable();
            $table->string('gender', 6)->comment('status of product: open, closed');
            $table->dateTime('date_of_birth')->nullable();
            $table->string('marital_status', 11)->nullable();
            $table->string('nationality', 100)->nullable();
            $table->string('location', 70)->nullable();
            $table->char('bloodgroup', 3)->nullable();
            $table->char('genotype', 2)->nullable();
            $table->mediumText('med_history')->nullable();
            $table->string('edu_level', 20)->nullable()->comment('highest level of education attained:');
            $table->string('grad_year', 4)->nullable()->comment('Year of graduation');
            $table->string('folio_num', 40)->nullable()->comment('Unique indoctrination number');
            $table->string('career_stage', 20)->nullable();
            $table->string('specialty', 100)->nullable();
            $table->string('sub_specialty', 100)->nullable();
            $table->string('licence_img_id', 100)->nullable();
            $table->string('run_hospital', 5)->nullable()->comment('indicates whether the doctor owns or runs a hospital');
            $table->unsignedInteger('med_school')->nullable();

            $table->index(["email"], 'email');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
