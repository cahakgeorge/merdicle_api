<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
*/

Route::group([
    'prefix'=>'v1',
    'namespace'=>'V1',
    ],function(){

    Route::POST('login', 'AuthController@authenticate');//->middleware('throttle');
    Route::GET('user-image/{image}', 'DataController@getProfilePic');
    Route::GET('user-thumb/{image}', 'DataController@getProfilePicThumb');
    
    Route::GET('data-image/{image}', 'DataController@getPostPic');
    Route::GET('data-video/{video}', 'DataController@getVideo');

    Route::GET('data-image/{type}/{id}', 'DataController@getDataPic');


    
    Route::prefix('in')->group(function(){
        //signup user
        Route::POST('signup/first', 'UserController@initUserSignup');
        Route::POST('signup', 'UserController@signupUser');
        Route::POST('token', 'UserController@resendToken');

        Route::POST('token/verification', 'UserController@verifyEmailToken');
        
        Route::POST('password/forgot', 'UserController@initiateForgotPassword');
        Route::POST('password/new', 'UserController@updatePasswordViaForgotToken');

        Route::GET('data', 'DataController@getOnboardData');
    });

    
    //protected routes
    Route::group(['middleware' => 'auth:api'], function(){

        Route::prefix('auths')->group(function(){
            //logout user
            Route::GET('logout', 'AuthController@logout');

            //Auth middleware redirects failed authentication here
            Route::GET('login', [ 'as' => 'login'], '');
        });

        Route::prefix('bio')->group(function(){
            Route::PATCH('doctor', 'UserController@updateDoctorBio');
            Route::PATCH('patient', 'UserController@updatePatientBio');
        });
        
        Route::prefix('account')->group(function(){
            //Route::GET('', 'UserController@getAccountOverview');
            Route::PATCH('password', 'UserController@updatePasswordFromProfile');
            
            Route::POST('follow', 'UserController@followUser');
            Route::DELETE('follow', 'UserController@unfollowUser');

            Route::DELETE('blockUser', 'UserController@blockUser');

            Route::prefix('profile')->group(function(){
                Route::GET('', 'UserController@getUserProfile');
                Route::GET('mine', 'UserController@getMyProfile');
                Route::PATCH('', 'UserController@updateUserProfile');
                Route::DELETE('', 'UserController@deleteUserAccount');
            }); 

            Route::prefix('contact')->group(function(){
                Route::GET('', 'UserController@getConnectionScreenData');
            });
            
            Route::GET('notification', 'UserController@getUserNotification');
            
        });


        Route::prefix('post')->group(function(){
            Route::GET('', 'UserController@getPosts');
            Route::GET('offset', 'UserController@getPosts');
            Route::POST('', 'UserController@createPost');

            Route::POST('like', 'UserController@likePost');
            Route::DELETE('like', 'UserController@unlikePost');
            
            Route::GET('comment', 'UserController@getPostComment');
            Route::POST('comment', 'UserController@createPostComment');
            Route::DELETE('comment', 'UserController@deletePostComment');

            Route::GET('bookmark', 'UserController@getBookmarkedPosts');
            Route::POST('bookmark', 'UserController@createPostBookmark');
            Route::DELETE('bookmark', 'UserController@deletePostBookmark');

            Route::GET('share', 'UserController@getUsersWhoSharedPost');
            Route::POST('share', 'UserController@sharePost');

            Route::GET('user', 'UserController@getAllPostsForUser');
        });

        Route::prefix('association')->group(function(){
            Route::GET('', 'DataController@getAssociations');
            //** */
            Route::POST('', 'DataController@createAssociation');

            Route::GET('post', 'DataController@getAssociationPosts');
            //Route::POST('post', 'DataController@createAssociationPosts'); //works same as creating a normal post same way

            Route::prefix('members')->group(function(){
                Route::GET('', 'DataController@getAssociationMembers');
                Route::POST('', 'UserController@joinAssociation');
                Route::DELETE('', 'UserController@leaveAssociation');
            });  
        });

        Route::prefix('case')->group(function(){
            Route::GET('', 'UserController@getCases');
            Route::POST('', 'UserController@createCase');

            Route::GET('review', 'UserController@getCaseReviews');
            Route::POST('review', 'UserController@createCaseReview');

            Route::POST('rating', 'UserController@createCaseRating');
        });  
        

        Route::prefix('hub')->group(function(){
            Route::GET('', 'DataController@getHubs');
            Route::POST('', 'DataController@createHub');
            Route::GET('post', 'DataController@getHubPosts');
            
            Route::prefix('members')->group(function(){
                Route::GET('', 'DataController@getHubMembers');
                Route::POST('', 'UserController@joinHub');
                Route::DELETE('', 'UserController@leaveHub');
            });  
        });

        Route::prefix('hospital')->group(function(){
            Route::GET('', 'DataController@getHospitals');
            Route::POST('', 'DataController@createHospital');
            Route::GET('nearby', 'DataController@getHospitalsNearby');
            Route::GET('address', 'DataController@getHospitalsViaAddress');
                            
            Route::prefix('review')->group(function(){
                Route::GET('', 'DataController@getHospitalReview');
                Route::POST('', 'UserController@createHospitalReview');
            });
        });

        Route::prefix('appoint')->group(function(){
            Route::POST('', 'UserController@createPatientAppointment');
            Route::GET('', 'UserController@getUserAppointments');
            Route::POST('action', 'UserController@approveAppointment');
                            
            Route::prefix('review')->group(function(){
                //Route::GET('', 'UserController@getHospitalReview');
                //Route::POST('', 'UserController@createHospitalReview'); reviews still call the post api
            });
        });
        

    });

});


  //TO GOD BE THE GLORY