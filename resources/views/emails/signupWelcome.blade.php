<!DOCTYPE html>
<html>
	<head>
		@include('includes.head')
	</head>
	<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

        @yield('welcome')
        
		@include('includes.footer')

	</body>
</html>
