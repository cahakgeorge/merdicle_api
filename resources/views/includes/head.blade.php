<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Redwoods Newsletter</title>

  <style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Nunito+Sans:700,300);

    body,
    #bodyTable,
    #bodyCell {
      height: 100% !important;
      margin: 0;
      padding: 0;
      width: 100% !important;
      margin-top: 2%;
      font-family: 'Nunito Sans', sans-serif;
      color: #556080;
    }

    table[class=themezyCaptionTopContent],
    table[class=themezyCaptionBottomContent] {
      width: 100% !important;
    }


    table[id=templateContainer],
    table[id=templatePreheader],
    table[id=templateHeader],
    table[id=templateBody],
    table[id=templateFooter] {
      max-width: 410px !important;
      width: 100% !important;
    }

    table {
      border-collapse: collapse;

    }



    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin: 0;
      padding: 0;
      font-family: 'Open Sans', sans-serif;
      font-weight: 400;
    }

    p {
      margin: 1em 0;
      padding: 0;
      font-family: 'Open Sans', sans-serif;
      font-size: 13px;
      line-height: 25px;
      color: #556080;
    }

    a {
      word-wrap: break-word;
    }

    table,
    td {

      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;

    }

    #outlook a {
      padding: 0;
    }

    img {
      -ms-interpolation-mode: bicubic;
    }

    body,
    table,
    td,
    p,
    a,
    li,
    blockquote {
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%;
    }

    #bodyCell {
      padding: 20px;
    }

    .themezyImage {
      vertical-align: bottom;
    }

    body,
    #bodyTable {
      background-color: #F2F2F2 !important;
    }

    #bodyCell {
      border-top: 0;
    }

    #templateContainer {
      border: 0;
    }

    .text-center {

      text-align: center;
      color: #556080;
    }


    .white-text {

      color: #556080;
      text-align: left;

      font-size: 24px;
      padding-top: 10px;
    }


    #templatePreheader {

      background-color: #F6F8FA;
      padding: 130px;
      border-top: 0;


    }

    .onboarding {
      background-image: url(/images/banner.png);
      background-size: cover;
      background-position: 50%;
      background-repeat: no-repeat;
      background-color: #F6F8FA;
      padding: 20px;
      min-height: 50px;
      max-width: 450px !important;
      border-top: 0;
     

    }

    .space {

      padding: 10px;
    }

    .login {
      color: #556080;
      text-decoration: none;
      font-weight: 900;
    }

    .preheaderContainer .themezyTextContent,
    .preheaderContainer .themezyTextContent p {
      color: #606060;
      font-size: 11px;
      line-height: 125%;
      text-align: left;
    }

    .preheaderContainer .themezyTextContent a {
      color: #606060;
      font-weight: normal;
      text-decoration: underline;
    }

    .themezyHeaderContent {

      font-size: 14px;
      padding-top: 9px;
      padding-right: 18px;
      padding-bottom: 9px;
      padding-left: 18px;

    }

    .themezyTextContent {

      font-size: 14px;
      padding-top: 19px;
      padding-right: 18px;
      padding-bottom: 19px;
      padding-left: 18px;
    }

    .themezyTextContent-2 {

      font-size: 14px;
      padding-top: 9px;
      padding-right: 18px;
      padding-bottom: 9px;
      padding-left: 18px;
    }

    .themezyButtonContent {

      font-size: 16px;
      padding: 16px 16px 40px 16px;


    }

    #templateHeader {
      background-color: #fff;
      border-top: 0;
      border-bottom: 0;
    }

    .headerContainer .themezyTextContent,
    .headerContainer .themezyTextContent p {
      color: #556080;
      
      line-height: 150%;
      text-align: left;
    }

    .headerContainer .themezyTextContent a {
      color: #6DC6DD;
      font-weight: normal;
      text-decoration: underline;
    }

    #templateBody {
      background-color: #ffffff;
      border-top: 0;
      border-bottom: 0;
    }

    .bodyContainer .themezyTextContent,
    .bodyContainer .themezyTextContent p {
      color: #606060;

      font-size: 15px;

      text-align: left;
    }

    .bodyContainer .themezyTextContent a {
      color: #60c1ae;
      font-weight: normal;
      text-decoration: none;
    }

    .themezyImageContent {

      padding-right: 9px;
      padding-left: 9px;
      padding-top: 25px;
      padding-bottom: 0;
      text-align: center;
    }

    #templateFooter {

      border-top: 0;
      border-bottom: 0;
    }

    .footerContainer .themezyTextContent-2,
    .footerContainer .themezyTextContent-2 p {
      color: #8f8f95;

      font-size: 11px;
      line-height: 25px;

      text-align: left;
    }

    .footerContainer .themezyTextContent a {
      color: #8f8f95;
      font-weight: normal;
      text-decoration: underline;
    }

    .banner {

      display: block;
      max-width: 100%;
      height: auto;
      margin: 0 auto;
    }

    .hr-class {

      border-top: 1px dashed #556080;
      opacity: 0.5;
    }

    .hr-class-two {

      border-top: 1px solid #556080;
      opacity: 0.5;
    }

    .text-center {

      text-align: center;
    }

    .address {

      padding-bottom: 50px;
    }

    .themezyButton {

      border-collapse: separate !important;
      border: 1px none #23afd9;
      border-radius: 5px;
      background-color: #23afd9;
      padding: 10px;
      margin-bottom: 20px !important;
      text-align: center;
      text-decoration: none;
      font-family: 'Nunito Sans', sans-serif;
      width: 100%;
      color: #FFFFFF;
      cursor: pointer;
    }



    @media only screen and (max-width: 480px) {

      body,
      table,
      td,
      p,
      a,
      li,
      blockquote {
        -webkit-text-size-adjust: none !important;
      }

      body {
        width: 100% !important;
        min-width: 100% !important;
      }

      td[id=bodyCell] {
        padding: 10px !important;
      }

      table[class=themezyTextContentContainer] {
        width: 100% !important;
      }

      img[class=themezyImage] {
        width: 100% !important;
      }

      table[class=themezyImageGroupContentContainer] {
        width: 100% !important;
      }

      table[class=themezyCaptionTopContent],
      table[class=themezyCaptionBottomContent] {
        width: 100% !important;
      }


      table[id=templateContainer],
      table[id=templatePreheader],
      table[id=templateHeader],
      table[id=templateBody],
      table[id=templateFooter] {
        max-width: 500px !important;
        width: 100% !important;
      }

      .themezyButton {

        width: 77%;
        font-size: 12px;
      }
    }
  </style>
