@extends('emails.signupWelcome')
@section('body')
<center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
      <tr>
        <td align="center" valign="top" id="bodyCell">
          <!-- BEGIN TEMPLATE // -->
          <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
            <tr>
              <td align="center" valign="top">
                <!-- BEGIN PREHEADER // -->
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="templatePreheader" id="sub-container">
                  <tr>
                    <td valign="top" class="themezyHeaderContent">
                      <center>
                        <h2>Logo</h2>
                      </center>
                    </td>
                  </tr>
                </table>
                <!-- // END PREHEADER -->
              </td>
            </tr>
            <tr>
            <tr>
              <td align="center" valign="top">
                <!-- BEGIN PREHEADER // -->
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="templatePreheader" id="sub-container">
                  <tr>
                    <td valign="top" class="space">
                      <center>

                      </center>
                    </td>
                  </tr>
                </table>
                <!-- // END PREHEADER -->
              </td>
            </tr>
            <tr>
              <td align="center" valign="top">
                <!-- ONBOARDING BACKGOUND IMAGE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="templatePreheader" class="onboarding">
                  <tr>
                    <td valign="top" class="themezyHeaderContent">

                    </td>
                  </tr>
                </table>
                <!-- // END PREHEADER -->
              </td>
            </tr>
            <tr>
              <td align="center" valign="top">
                <!-- BEGIN HEADER // -->
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                  <tr>
                    <td valign="top" class="headerContainer">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyImageBlock">
                        <tbody class="themezyImageBlockOuter">
                          <tr>
                            <td class="themezyImageContent" valign="top">
                            </td>
                          </tr>
                          <!-- // BODY TEXT -->
                          <tr>
                            <td valign="top" class="themezyTextContent">
                              <div class="text-center">
                                <p class="white-text">Hi <strong><span>{{$name}}</span></strong>,</p>
                                <p>Great to have you here, I see you have completed your signup. </p>
                                <p> Go right ahead into your app and join other exclusive winners like yourself. </p>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td valign="middle" class="themezyButtonContent">
                              <!-- // BUTTON -->
                              <p>Kind Regards,</p>
                              <h3><strong>Bernard Rim-Karim</strong></h3>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>

            @endsection