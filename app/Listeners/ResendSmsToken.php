<?php

namespace App\Listeners;

use App\Events\AccountPhoneUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Jobs\Sms;

use App\Jobs\LogToSlack;
use Carbon\Carbon;

/**
 * Send Onboard Notification.
 * After user account creation
 * 
 * @return void
 */
class ResendSmsToken
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  AccountPhoneUpdated  $event
     * @return void
     */
    public function handle(AccountPhoneUpdated $event)
    {
        //dispatch(new LogToSlack('Message sent to phone: '.$event->recipient.' :: '.$event->message))->delay(Carbon::now()->addSeconds(5));
        
        //push sms to recipient using a job
        dispatch(new Sms($event->recipient, $event->message));
    }

}
