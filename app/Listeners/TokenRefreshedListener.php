<?php

namespace App\Listeners;

use App\Events\RefreshEmailToken;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use App\Repositories\VerifyUserRepository;
use App\Mail\TokenRefreshed;

/**
 * Send new token
 * After user refreshes his email link
 * 
 * @return void
 */
class TokenRefreshedListener
{
    protected $verifyUserRepository;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(VerifyUserRepository $verifyUserRepository)
    {
        $this->verifyUserRepository = $verifyUserRepository;
    }
    
    /**
     * Handle the event.
     *
     * @param  RefreshEmailToken  $event
     * @return void
     */
    public function handle(RefreshEmailToken $event)
    {
        //send email to user
        Mail::to($event->user->email)->send(new TokenRefreshed($event->name, $event->user->email, 
                    $event->token));
    }

}
