<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue; //implement this to make class always queued

//use App\Jobs\LogToSlack;


class SignupWelcome extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name)
    {
        $this->name = $name;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {        
        return $this->view('includes.welcome');
    }
}