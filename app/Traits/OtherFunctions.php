<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

use Image;
use Storage;

trait OtherFunctions
{
    public function generateToken(): string{
        return sprintf("%06d", mt_rand(1111, 9999));
    }

    public function getPhoneInStandardSmsFormat($phone, $dialCode){
        if(substr($phone, 0, 1) == '0') $phone = substr($phone, 1);
        if(substr($dialCode, 0, 1) == '+') $dialCode = substr($dialCode, 1);
        
        return $dialCode.$phone;
    }

    public function getTokenMessage($token): string{
        return "Hi, \nyour merdicle verification token is ".$token." \nWelcome..";
    }

 
    public function generate_string($input, $strength = 16) {
        $input_length = strlen($input);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }

        return $random_string;
    }

    public function generatePayRef(): string{
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return 'BD-'.generate_string($permitted_chars, 8).'-IN';
    }

    public function getInitializeCardAmount(): int{ //amount in naira, convert to kobo
        return 100;
    }

    public function generateEmailToken(): string{
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return generate_string($permitted_chars, 60);
    }

    public function getEmailLink(int $tokenId, string $emailToken): string{
        $emailLink = env('APP_URL', 'https://merdicle.com')."/api/v1/email/verification?tid=$tokenId&ut=$emailToken";

        return $emailLink;
    }



    public function saveProfilePhoto($request): string{
        if (filter_var($request->profile_pic, FILTER_VALIDATE_URL)) 
        {
            $name = $request->profile_pic;
        }
        else{
            if($request->hasFile('image') && $request->image->isValid()){
                //we would be saving the users post pic wiv id.extension
                //here store first parameter is null cos 'postPics' leads directly to the needed folder.
                //extension
                $randNum = mt_rand(1000, 10000000);
                $ext = $request->file('image')->extension();
                $currentTime = strtotime('now');
                $name = 'avatar_'. $randNum ."_".$currentTime.".".$ext;
                $path = $request->file('image')->storeAs('profile_pics', $name, 'public'); 
                
                $originalImage = $request->file('image');
                $thumbnailImage = Image::make($originalImage);

                $thumbnailImage->resize(300, null, function($constraint){
                    $constraint->aspectRatio();
                });
                
                $thumbnailPath = 'public/profile_pics/thumbnails/';

                Storage::put($thumbnailPath.$name, $thumbnailImage->encode());
                //$thumbnailImage->storeas($thumbnailPath.$name);
            }
        }

        return $name;
    }

    public function saveOtherPhoto(int $userId, $request, string $photoField): string{
        if (filter_var($request->$photoField, FILTER_VALIDATE_URL)) 
        {
            return $name = $request->$photoField;
        }
        else{
            if($request->hasFile($photoField) && $request->$photoField->isValid()){
                //we would be saving the users profile pic wiv id.extension
                //here store first paremeter is null cos 'postPics' leads directly to the needed folder.
                //extension
                $randNum = mt_rand(10,1000);
                $ext = $request->file($photoField)->extension();
                $currentTime = date("his");
                $name = 'photo_'. $userId . $randNum . $currentTime . ".".$ext;
                $path = $request->file($photoField)->storeAs('profile_pics', $name, 'public');
                
                return $name;
            }
        }

    }

    public function saveOtherFiles(int $userId, $request, string $fileField, string $storagePath): string{
        if (filter_var($request->$fileField, FILTER_VALIDATE_URL)) 
        {
            return $name = $request->$fileField;
        }
        else{
            if($request->hasFile($fileField) && $request->$fileField->isValid()){
                //we would be saving the users file
                $randNum = mt_rand(10,1000);
                $ext = $request->file($fileField)->extension();
                $currentTime = date("his");
                $name = 'post_'. $userId . $randNum . $currentTime . ".".$ext;
                $path = $request->file($fileField)->storeAs($storagePath, $name, 'public');
                
                return $name;
            }
        }
    }

    //for sending push messages
    public function sendPushMessage($data, $target, $type)
    {
        //FCM api URL, retrieve from config variable
        $fcmUrl = config('extconfig.fcmUrl');         			
        $fields = array();
        $fields['data'] = $notif = ['title'=>"Vybe", 'body'=>$data, 'content_available'=>true, 'priority'=>"high", 'type'=>$type];
        
        if(is_array($target)) $fields['registration_ids'] = $target;
        else $fields['to'] = $target;
        
        $networkHelper = new NetworkHelper();
        
        //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
        //header with content_type api key, retrieve api key from Config/extconfig file
        //https://laracasts.com/discuss/channels/general-discussion/ho-to-access-config-variables-in-laravel-5
        $headers = [ 'Content-Type:application/json', 'Authorization:key='.config('extconfig.firebaseApiKey') ];

        //use a helper class for network calls
        return $networkHelper->sendPushMessage($fcmUrl, $headers, $fields);
    }

    /**
 * Calculates the great-circle distance between two points, with
 * the Vincenty formula.
 * @param float $latitudeFrom Latitude of start point in [deg decimal]
 * @param float $longitudeFrom Longitude of start point in [deg decimal]
 * @param float $latitudeTo Latitude of target point in [deg decimal]
 * @param float $longitudeTo Longitude of target point in [deg decimal]
 * @param float $earthRadius Mean earth radius in [m]
 * @return float Distance between points in [m] (same as earthRadius)
 */
public static function vincentyGreatCircleDistance(
    $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
  {
    // convert from degrees to radians
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);
  
    $lonDelta = $lonTo - $lonFrom;
    $a = pow(cos($latTo) * sin($lonDelta), 2) +
      pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
    $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);
  
    $angle = atan2(sqrt($a), $b);
    return $angle * $earthRadius;
  }

}