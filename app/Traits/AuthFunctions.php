<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;


trait AuthFunctions
{
    protected $statusArray = ['0'=>'Please click on the link sent to your registered email address', '2'=>'Account has been suspended', '3'=>'User account is inactive'];

    public function verifyUserIsInCategory($userCategory, $categoryToMatchAgainst){
        if($userCategory !=  $categoryToMatchAgainst)
            throw new HttpResponseException(response()->json(["status"=>false,"data"=>null,"message"=>'Invalid user category'], 412));
    }

    public function invalidatePreviousTokens(object $user){
        $userTokens = $user->tokens ?? [];

        for($i=0; $i<sizeof($userTokens); $i++){
            if($i != 0 )$userTokens[$i]->revoke();   //the first token is the latest token
        }
    }

    public function invalidateAllUserTokens(object $user){
        $userTokens = $user->tokens ?? [];
        
        for($i=0; $i<sizeof($userTokens); $i++){
            $userTokens[$i]->revoke();   //the first token is the latest token
        }
    }
    
    /**
     * Get different token expiry periods for different user categories
     */
    public function getExpiryDateForUserToken($user): object{
        $roles = $user->getRoleNames();
        
        foreach($roles as $role){ //run through each role present, if any matches switch condition, return expiry period for that
            switch($role){
                case 'retailer' : return Carbon::now()->addDay();
                case 'bank-admin' : return Carbon::now()->addDay();
                case 'bank-dealer' : return Carbon::now()->addYear();
                case 'super-admin-r' : return Carbon::now()->addDay();
                case 'corporate-admin' : return Carbon::now()->addDay();
                case 'corporate-investment-manager' : return Carbon::now()->addDays(5);
            }
        }
    
        return Carbon::now();
    }

    public function getTokenString(): string{
        return str_random(40);
    }
    
    /**
     * Verify if user(admin) has sufficient priviledges
     */
    public function isUserPermittedToMakeRequest(object $adminUser, object $userToBeResetDetails, $userToBeResetRoles){
        //check if admin has permission to manage first user role  &&  //next check if admin is admin of that particular user
        return $adminUser->hasPermissionTo("reset $userToBeResetRoles[0]") && $this->isAdminOfUserToBeReset($adminUser, $userToBeResetDetails, $adminIdField='admin_id');
    }

    /**
     * Verify if user(admin) has sufficient priviledges
     */
    public function isUserPermittedToMakeRestrictRequest(object $adminUser, object $userToBeResetDetails, $userToBeResetRoles){
        //check if admin has permission to manage first user role  &&  //next check if admin is admin of that particular user
        return $adminUser->hasPermissionTo("reset $userToBeResetRoles[0]") && $this->isAdminOfUserToBeReset($adminUser, $userToBeResetDetails, $adminIdField='admin_id');
    }

    /**
     * Verify if user(admin) has sufficient priviledges
     */
    public function isUserPermittedToMakeDeleteRequest(object $adminUser, object $userToBeResetDetails, $userToBeResetRoles){
        //check if admin has permission to manage first user role  &&  //next check if admin is admin of that particular user
        return $adminUser->hasPermissionTo("reset $userToBeResetRoles[0]") && $this->isAdminOfUserToBeReset($adminUser, $userToBeResetDetails, $adminIdField='admin_id');
    }
    
    /**
     * Verify admin is admin of user to be reset
     */
    public function isAdminOfUserToBeReset(object $adminUser, object $userToBeResetDetails, $adminIdField): bool{
        return ( (!empty($adminIdField) && ($userToBeResetDetails->admin_id == $adminUser->id)) || ($adminUser->getRoleNames()->toArray()[0])=='super-admin-r' );
    }

}