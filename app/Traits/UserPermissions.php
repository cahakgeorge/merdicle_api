<?php

namespace App\Traits;

trait UserPermissions
{
    /**
    * Can admin view dashboard?
    * @return array 
    */
    public function canAdminViewDashboard(object $user): bool{
        return $user->can('view dashboard') ?? false;
    }
    
    
}