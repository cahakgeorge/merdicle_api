<?php

namespace App\Exceptions;

use Exception;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Illuminate\Support\Facades\Log;

//LINE 78 render function switches responses between api calls and web route calls
//https://laracasts.com/discuss/channels/requests/custom-exception-handler-based-on-route-group
use Symfony\Component\HttpFoundation\Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof \Spatie\Permission\Exceptions\UnauthorizedException) {
            return response()->json([
                'status'=>false,
                'data'=>null,
                'message'=>"Unauthorized",
            ], Response::HTTP_OK); //Response::HTTP_UNAUTHORIZED
        }
        
        //upload max exceeded
        if($exception instanceof  \Illuminate\Http\Exceptions\PostTooLargeException ){
            return response()->json([
                'status'=>false,
                'data'=>null,
                'message'=>'Filesize too large',
            ], Response::HTTP_OK);//Response::HTTP_REQUEST_ENTITY_TOO_LARGE
        }

        //database field type restriction exceeded
        if($exception instanceof  \Illuminate\Database\QueryException ){
            return response()->json([
                'status'=>false,
                'data'=>null,
                'message'=> "Query Error".$exception->getMessage()//'Db Error',
            ], Response::HTTP_OK);
        }

        $namespacedClassName = get_class($exception);
        if($request->is('api/*')){
            return response()->json([
                'status'=>false,
                'data'=>null,
                'message'=>"Exception: ".$exception->getMessage(), //close this once done testing
            ], Response::HTTP_OK);//Response::HTTP_BAD_REQUEST  //Symfony\\Component\\HttpKernel\\Exception\\HttpException
        }
        
        return parent::render($request, $exception);
    }
}
