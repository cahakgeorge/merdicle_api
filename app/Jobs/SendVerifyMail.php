<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

//Exception 
use Exception;
use Error;

use Illuminate\Support\Facades\Log;

//Mailable class
use App\Mail\VerifyMail;

class SendVerifyMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $name;
    public $email;
    public $token;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($name, $email, $token)
    {
        $this->name = $name;
        $this->email = $email;
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            Mail::to($this->email)->send(new VerifyMail($this->name, $this->email,  $this->token));
        }catch(Exception $ex){
            Log::error(json_encode($ex->getMessage()));
        }
    }
}
