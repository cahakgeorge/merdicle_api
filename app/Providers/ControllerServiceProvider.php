<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ControllerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //require all classes in the Controller Contracts directory
        foreach (glob(app_path() . '/Http/Controllers/V1/Contracts*.php') as $file) {
            require_once($file);
        }
    }
}
