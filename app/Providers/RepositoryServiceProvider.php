<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //require all classes in the Repositories directory
        foreach (glob(app_path() . '/Repositories/*.php') as $file) {
            require_once($file);
        }
    }
}
