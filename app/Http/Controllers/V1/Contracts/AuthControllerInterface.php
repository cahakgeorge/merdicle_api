<?php

namespace App\Http\Controllers\V1\Contracts;

use Illuminate\Http\Request;
use App\Repositories\{
    UserRepository
};

use App\Http\Requests\{
    AuthUserRequest
};


interface AuthControllerInterface
{
    public function authenticate(AuthUserRequest $request,  UserRepository $userRepository);
                                                            
    public function logout(Request $request);
}

