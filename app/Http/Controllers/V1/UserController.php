<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;


use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Exceptions\HttpResponseException;

use App\Http\Requests\{
                            FirstSignupUserRequest, SignupUserRequest,
                            VerifyEmailTokenRequest, ForgotPasswordRequest, UpdatePasswordRequest,
                            UpdatePatientBio, UpdateDoctorBio, GetUserProfileRequest, UpdateUserProfileRequest,
                            CreatePostRequest, PostIdRequest, CreatePostCommentRequest, CreatePostShareRequest,
                            UserIdRequest, CreateCaseReviewRequest, GetCaseReviewRequest,
                            CreateCaseRequest, CreateCaseRatingRequest, AssociationIdRequest, HubIdRequest,
                            CreatePatientAppointmentRequest, EditAppointmentRequest, VerifyForgotPasswordTokenRequest,
                            CreateHospitalReviewRequest, AppointmentIdRequest
                        };

use App\Repositories\{
                            UserRepository, DataRepository
                        };


use UserHelper;
use DataHelper;
use App\Http\Controllers\V1\Contracts\UserControllerInterface;

/**
 * @group User Functions
 *
 * APIs for managing user's functionalities
 */

class UserController extends Controller implements UserControllerInterface
{

    /**
     * First user signup request
     * @authenticated
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function initUserSignup(FirstSignupUserRequest $request,
                                        UserRepository $userRepository)
    {   
        return response()->json(['status'=>true,'data'=>null, 'message'=>''], Response::HTTP_OK);
    }

    /**
     * Signup a user
     * @authenticated
     * @response {
     *       "status": true,
     *       "data": {
     *               "token": "$token",
     *               "name": "$name"
     *           },
     *       "message": null
     *  }
     */
    public function signupUser(SignupUserRequest $request,
                                        UserRepository $userRepository)
    {
        $userHelper = new UserHelper();   
        if(($data = $userHelper->signupUser($request, $userRepository)))
                                return response()->json($data, Response::HTTP_OK);
    }


    /**
     * Verify email token
     * @authenticated
     * @response {
     *       "status": true,
     *       "data":null,
     *       "message": null
     *  }
     */
    public function verifyEmailToken(VerifyEmailTokenRequest $request,
                                        UserRepository $userRepository)
    {
        $userHelper = new UserHelper();   
        if(($data = $userHelper->verifyEmailToken($request, $userRepository)))
             return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Resend email token
     * @authenticated
     * @response {
     *       "status": true,
     *       "data": {
     *               "id": "$id",
     *           },
     *       "message": null
     *  }
     */
    public function resendToken(SignupUserRequest $request,
                                        UserRepository $userRepository)
    {
        $userHelper = new UserHelper();   
        if(($data = $userHelper->signupUser($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }


    /**
     * Forgot Password
     * send a reset token to both the phone and email address of user
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function initiateForgotPassword(ForgotPasswordRequest $request,
                                        UserRepository $userRepository)
    {
        $userHelper = new UserHelper();
        if(($data = $userHelper->initiateForgotPassword($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }
    
    /**
     * Update password from forgot password
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function updatePasswordViaForgotToken(VerifyForgotPasswordTokenRequest $request,
                                                UserRepository $userRepository)
    {
        $userHelper = new UserHelper();
        if(($data = $userHelper->updatePasswordViaForgotToken($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    //  ??????????????????????????????????????? FIRST LOGIN ???????????????????????????????????????
    /**
     * Update patient bio
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function updatePatientBio(UpdatePatientBio $request,
                                                UserRepository $userRepository)
    {
        $userHelper = new UserHelper();
        if(($data = $userHelper->updatePatientBio($request, $userRepository)))         return response()->json($data, Response::HTTP_OK);
    }
    
    /**
     * Update doctor bio
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     * }
     */
    public function updateDoctorBio(UpdateDoctorBio $request,
                                                UserRepository $userRepository)
    {
        $userHelper = new UserHelper();
        if(($data = $userHelper->updateDoctorBio($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }




    //  ??????????????????????????????????????? AFTER LOGIN ???????????????????????????????????????
    
    /**
     * Get overview of user account
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getAccountOverview(Request $request, UserRepository $userRepository)
    {
        $userHelper = new UserHelper();
        if(($data = $userHelper->getAccountOverview($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Update password from profile
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function updatePasswordFromProfile(UpdatePasswordRequest $request,
                                                UserRepository $userRepository)
    {
        $userHelper = new UserHelper();
        if(($data = $userHelper->updatePasswordFromProfile($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Get user profile
     * Get profile of user with id supplied
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getUserProfile(GetUserProfileRequest $request,
                                        UserRepository $userRepository)
    {
        $userHelper = new UserHelper();
        if(($data = $userHelper->getUserProfile($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Get current users profile
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getMyProfile(UserRepository $userRepository)
    {
        $userHelper = new UserHelper();
        if(($data = $userHelper->getMyProfile($userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Update user profile
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function updateUserProfile(UpdateUserProfileRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->updateUserProfile($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

     /**
     * Delete user account
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function deleteUserAccount(UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->deleteUserAccount($userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Get posts
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getPosts(UserRepository $userRepository)
    {
        $userHelper = new UserHelper();
        if(($data = $userHelper->getPosts($userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Get all posts made by a single user
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getAllPostsForUser(UserIdRequest $request, UserRepository $userRepository)
    {
        $userHelper = new UserHelper();
        if(($data = $userHelper->getAllPostsForUser($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }
    
    /**
     * Create user post
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function createPost(CreatePostRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->createPost($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    
    /**
     * Like post
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function likePost(PostIdRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->likePost($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Unlike post
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function unlikePost(PostIdRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->unlikePost($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Get post comment
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getPostComment(PostIdRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->getPostComment($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Create post comment
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function createPostComment(CreatePostCommentRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->createPostComment($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Delete post comment
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function deletePostComment(PostIdRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->deletePostComment($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Get post bookmarks
     * Get all posts bookmarked by user
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getBookmarkedPosts(UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->getBookmarkedPosts($userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Create post comment
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function createPostBookmark(PostIdRequest $request,
                                        UserRepository $userRepository)
    {
        $userHelper = new UserHelper();
        if(($data = $userHelper->createPostBookmark($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }
    
    /**
     * Delete post comment
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function deletePostBookmark(PostIdRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->deletePostBookmark($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Get users who shared post
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getUsersWhoSharedPost(PostIdRequest $request, UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->getUsersWhoSharedPost($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Share post
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function sharePost(CreatePostShareRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->sharePost($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }


    /**
     * Get user notifications
     * Get all posts bookmarked by user
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getUserNotification(UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->getUserNotifications($userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Follow user
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function followUser(UserIdRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->followUser($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Unfollow user
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function unfollowUser(UserIdRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->unfollowUser($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Block user
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function blockUser(UserIdRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->blockUser($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Get list of cases
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getCases(UserRepository $userRepository)
    {
        $userHelper = new UserHelper();
        if(($data = $userHelper->getCases($userRepository))) return response()->json($data, Response::HTTP_OK);
    }
    
    /**
     * Create case
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function createCase(CreateCaseRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->createCase($request, $userRepository))) return response()->json($data, Response::HTTP_OK); 
    }

    /**
     * Get case reviews
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getCaseReviews(GetCaseReviewRequest $request, UserRepository $userRepository)
    {
        $userHelper = new UserHelper();
        if(($data = $userHelper->getCaseReviews($request, $userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Create case review
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function createCaseReview(CreateCaseReviewRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->createCaseReview($request, $userRepository))) return response()->json($data, Response::HTTP_OK); 
    }

    /**
     * Create case rating
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function createCaseRating(CreateCaseRatingRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->createCaseRating($request, $userRepository))) return response()->json($data, Response::HTTP_OK); 
    }

    /**
     * Create hospital review
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function createHospitalReview(CreateHospitalReviewRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->createHospitalReview($request, $userRepository))) return response()->json($data, Response::HTTP_OK); 
    }

    /**
     * Get contacts data
     * @response {
     * "status": true,
     *  "data": {
     *               "token": "$token",
     *               "name": "$name"
     *           },
     *  "message": null
     * }
	 */
    public function getConnectionScreenData(UserRepository $userRepository)
    {   
        $userHelper = new UserHelper(); 

        if(($data = $userHelper->getConnectionScreenData($userRepository))) return response()->json($data, Response::HTTP_OK);
    }  


    /**
     * Join association
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function joinAssociation(AssociationIdRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->joinAssociation($request, $userRepository))) return response()->json($data, Response::HTTP_OK); 
    }

    /**
     * Leave association
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function leaveAssociation(AssociationIdRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->leaveAssociation($request, $userRepository))) return response()->json($data, Response::HTTP_OK); 
    }

    /**
     * Join hub
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function joinHub(HubIdRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->joinHub($request, $userRepository))) return response()->json($data, Response::HTTP_OK); 
    }

    /**
     * Leave hub
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function leaveHub(HubIdRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->leaveHub($request, $userRepository))) return response()->json($data, Response::HTTP_OK); 
    }


    /**
     * Create appointment with doctor
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */ 
    public function createPatientAppointment(CreatePatientAppointmentRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->createPatientAppointment($request, $userRepository))) return response()->json($data, Response::HTTP_OK); 
    }

    /**
     * Get user appointments
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getUserAppointments(UserRepository $userRepository)
    {
        $userHelper = new UserHelper();
        if(($data = $userHelper->getUserAppointments($userRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Approve appointment
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function approveAppointment(AppointmentIdRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->approveAppointment($request, $userRepository))) return response()->json($data, Response::HTTP_OK); 
    }

    /**
     * Edit appointment
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function editAppointment(EditAppointmentRequest $request,
                                        UserRepository $userRepository)
    {   
        $userHelper = new UserHelper();
        if(($data = $userHelper->editAppointment($request, $userRepository))) return response()->json($data, Response::HTTP_OK); 
    }
}
