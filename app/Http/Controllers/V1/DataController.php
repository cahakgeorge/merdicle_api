<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;


use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Exceptions\HttpResponseException;

use App\Http\Requests\{
                            AssociationIdRequest, HubIdRequest, CreateAssociationRequest, HospitalIdRequest,
                            CreateHospitalRequest, SearchForHospitalRequest, CreateHubRequest
                        };

use App\Repositories\{
                            DataRepository
                        };

use UserHelper;
use DataHelper;
use App\Http\Controllers\V1\Contracts\DataControllerInterface;

/**
 * @group Data Functions
 *
 * APIs for managing vital data needed by app
 */

class DataController extends Controller implements DataControllerInterface
{

    /**
     * Get onboard data such as countries, specialties
     * @authenticated
     * @response {
     *       "status": true,
     *       "data": {
     *               "product": $products[],
     *           },
     *       "message": null
     *  }
     */
    public function getOnboardData(DataRepository $dataRepository)
    {
        $dataHelper = new DataHelper();   
        if(($data = $dataHelper->getOnboardData($dataRepository)))
                                                return response()->json($data, Response::HTTP_OK);
    }
   

    /**
     * Create association
     * @authenticated
     * @response {
     *       "status": true,
     *       "data":null,
     *       "message": null
     *  }
     */
    public function createAssociation(CreateAssociationRequest $request,
                                        DataRepository $dataRepository)
    {
        $dataHelper = new DataHelper();
        if(($data = $dataHelper->createAssociation($request, $dataRepository)))
                return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Get list of associations
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getAssociations(DataRepository $dataRepository)
    {
        $dataHelper = new DataHelper();   
        if(($data = $dataHelper->getAssociations($dataRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Get association posts
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getAssociationPosts(AssociationIdRequest $request, DataRepository $dataRepository)
    {   
        $dataHelper = new DataHelper();
        if(($data = $dataHelper->getAssociationPosts($request, $dataRepository))) return response()->json($data, Response::HTTP_OK);
    }


    /**
     * Get association members
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getAssociationMembers(AssociationIdRequest $request, DataRepository $dataRepository)
    {   
        $dataHelper = new DataHelper();
        if(($data = $dataHelper->getAssociationMembers($request, $dataRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Create hub
     * @authenticated
     * @response {
     *       "status": true,
     *       "data":null,
     *       "message": null
     *  }
     */
    public function createHub(CreateHubRequest $request,
                                        DataRepository $dataRepository)
    {
        $dataHelper = new DataHelper();
        if(($data = $dataHelper->createHub($request, $dataRepository)))
                return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Get list of hubs
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getHubs(DataRepository $dataRepository)
    {
        $dataHelper = new DataHelper();   
        if(($data = $dataHelper->getHubs($dataRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Get hub posts
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getHubPosts(HubIdRequest $request, DataRepository $dataRepository)
    {   
        $dataHelper = new DataHelper();
        if(($data = $dataHelper->getHubPosts($request, $dataRepository))) return response()->json($data, Response::HTTP_OK);
    }


    /**
     * Get hub members
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getHubMembers(HubIdRequest $request, DataRepository $dataRepository)
    {   
        $dataHelper = new DataHelper();
        if(($data = $dataHelper->getHubMembers($request, $dataRepository))) return response()->json($data, Response::HTTP_OK);
    }


    /**
     * Create hospital
     * @authenticated
     * @response {
     *       "status": true,
     *       "data":null,
     *       "message": null
     *  }
     */
    public function createHospital(CreateHospitalRequest $request,
                                        DataRepository $dataRepository)
    {
        $dataHelper = new DataHelper();
        if(($data = $dataHelper->createHospital($request, $dataRepository)))
                                return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Get list of hospitals
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getHospitals(DataRepository $dataRepository)
    {
        $dataHelper = new DataHelper();   
        if(($data = $dataHelper->getHospitals($dataRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Get list of hospitals closeby
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getHospitalsNearby(SearchForHospitalRequest $request, DataRepository $dataRepository)
    {
        $dataHelper = new DataHelper();   
        if(($data = $dataHelper->getHospitalsNearby($request, $dataRepository))) return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Get list of hospitals matching address
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getHospitalsViaAddress(SearchForHospitalRequest $request, DataRepository $dataRepository)
    {
        $dataHelper = new DataHelper();   
        if(($data = $dataHelper->getHospitalsViaAddress($request, $dataRepository))) return response()->json($data, Response::HTTP_OK);
    }
    

    /**
     * Get hospital reviews
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getHospitalReview(HospitalIdRequest $request, DataRepository $dataRepository)
    {   
        $dataHelper = new DataHelper();
        if(($data = $dataHelper->getHospitalReview($request, $dataRepository))) return response()->json($data, Response::HTTP_OK);
    }












    /**
     * Get profile picture
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getProfilePic(string $imageName, DataRepository $dataRepository)
    {   
        $dataHelper = new DataHelper();
        return $dataHelper->getProfilePic($imageName);
    }


    /**
     * Get profile picture thumb
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getProfilePicThumb(string $imageName, DataRepository $dataRepository)
    {   
        $dataHelper = new DataHelper();
        return $dataHelper->getProfilePicThumb($imageName);
    }

    /**
     * Get post picture
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getPostPic(string $imageName, DataRepository $dataRepository)
    {   
        $dataHelper = new DataHelper();
        return $dataHelper->getVidPicFile($imageName);
    }

    /**
     * Get video
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getVideo(string $link, DataRepository $dataRepository)
    {   
        $dataHelper = new DataHelper();
        return $dataHelper->getVidPicFile($link);
    }

    /**
     * Get data picture
     * eg get type is post, id is post picture
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getDataPic(string $type, string $typeId, DataRepository $dataRepository)
    {   
        $dataHelper = new DataHelper();
        return $dataHelper->getVidPicFile($imageName);
    }
    
}   
