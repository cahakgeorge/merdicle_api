<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Repositories\{
    UserRepository
};

use AuthHelper;
use App\Http\Requests\{
                        AuthUserRequest
                    };


use App\Http\Controllers\V1\Contracts\AuthControllerInterface;

/**
 * 
 * 
 * @group User authentication
 *
 * APIs for managing authentication, and resets
 */
                                                           
class AuthController extends Controller implements AuthControllerInterface
{
    
    public function __construct()
	{
        $this->authHelper = new AuthHelper(); 
    }
    
    /**
     * Authenticate User Login
     * via credentials entered on platform
     * @bodyParam email string required The users email
     * @bodyParam password string required The users password
     * @response {
     * "status": true,
     *  "data": {
     *               "token": "$token",
     *               "name": "$name"
     *           },
     *  "message": null
     * }
	 */
    public function authenticate(AuthUserRequest $request,  UserRepository $userRepository)
    {   //Validated data present
        if(($response = $this->authHelper->authenticateUser($request, $userRepository))) 
                                                             return response()->json($response, Response::HTTP_OK);
    }
    
    
    /**
     * Logout user
     * Logout user and revoke access token
     * @response {
     *      "status": true,
     *      "data": null,
     *      "message": ''
     * }
     * * @return string 
     */
    public function logout(Request $request)
    {
        $status = $this->authHelper->logoutUser($request);

        if(($response = $this->authHelper->logoutUser($request))) return response()->json($response, Response::HTTP_OK);
    }
}
