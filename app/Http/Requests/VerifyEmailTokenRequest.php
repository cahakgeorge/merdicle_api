<?php

namespace App\Http\Requests;

/**
* Validate bank custodian request
* During use signup
*/
use App\Http\Requests\Contracts\GeneralRequestInterface;

use Exception;

class VerifyEmailTokenRequest extends BaseFormRequest implements GeneralRequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required_without:phone|email|unique:user,email|between:6,90',
            'first_name' => 'bail|required|alpha_dash|max:100',
            'last_name' => 'bail|required|alpha_dash|max:100',
            'gender' => 'required|alpha',
            'password' => 'bail|required|string|between:4,100',
            
            'image' => 'required|image',
            'dob' => 'required|date',
            'nationality' => 'required|alpha|between:0,100',
            'mstatus' => 'required|alpha|between:5,40',
            'signup_type'=>'required|alpha|between:3,10',
            
            'token' => 'required|numeric|digits_between:4,6',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'integer'      => 'The :attribute must have an integer value',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters(): array
    {
        return [
        ];
    }   
}
