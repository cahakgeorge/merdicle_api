<?php

namespace App\Http\Requests;

/**
* Validate bank custodian request
* During use signup
*/
use App\Http\Requests\Contracts\GeneralRequestInterface;

use Exception;

class CreateCaseRatingRequest extends BaseFormRequest implements GeneralRequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'case_id' => 'required|integer|exists:cases,id|min:1',
            'rating' => 'required|numeric|between:0,5',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'integer'      => 'The :attribute must have an integer value',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters(): array
    {
        return [
        ];
    }   
}
