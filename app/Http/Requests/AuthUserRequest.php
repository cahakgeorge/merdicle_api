<?php

namespace App\Http\Requests;

/**
 * Validate request to authenticate user
 *
 */

use Illuminate\Validation\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response;

use App\Http\Requests\Contracts\AuthUserRequestInterface;

use Exception;

class AuthUserRequest extends BaseFormRequest implements AuthUserRequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email|max:191',
            'password' => 'required|string|max:100',
        ];
        
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'email.required' => 'Email is required!',
            'password.required' => 'Password is required!',
            'max'      => 'The :attribute must have a maximum value of :max',
            'string'      => 'The :attribute must have a string value',
            'email'      => 'The :attribute must be a valid email',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters(): array
    {
        return [
        ];
    }   
}
