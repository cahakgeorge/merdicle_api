<?php

namespace App\Http\Requests;

/**
 * Base Validation rules for forms
 */

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Waavi\Sanitizer\Laravel\SanitizesInput;
use Illuminate\Validation\ValidationException;

use App\Http\Requests\Contracts\BaseFormRequestInterface;


abstract class BaseFormRequest extends FormRequest implements BaseFormRequestInterface
{   
    //use ApiResponse, 
    use SanitizesInput;
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    abstract public function rules(): array;
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    abstract public function authorize(): bool;

     /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *  * For more sanitizer rule check https://github.com/Waavi/Sanitizer
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $this->sanitize();
        //parent::failedValidation($validator);

        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(["status"=>false,"data"=>null,"message"=>$this->extractValidationErrors($errors)], JsonResponse::HTTP_OK));

        /* 
            abort(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, $this->extractValidationErrors($errors)); */
        
        /* throw new HttpResponseException(response()->json(['errors' => $errors
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)); */
    }

    /**
     * Extract first validation error from 
     * error array
     *
     * @param array
     * @return string
     */
    public function extractValidationErrors(array $errors): string{
        $errorArrayKeys = array_keys($errors);

        return $errorMessage = $errors[$errorArrayKeys[0]][0];
    }
}
