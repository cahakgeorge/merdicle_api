<?php

namespace App\Http\Requests;

/**
* Validate bank custodian request
* During use signup
*/
use App\Http\Requests\Contracts\GeneralRequestInterface;

use Exception;

class EditAppointmentRequest extends BaseFormRequest implements GeneralRequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'appoint_id' => 'required|integer|exists:appointment,id|min:1',
            'date' => 'required|date',
            'address' => 'required|alpha_dash|between:0,100',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'required' => 'The :attribute field is required',
            'date'    => ':attribute must be a date',
            'between' => 'The :attribute must be between :min - :max.',
            'alpha_dash'      => 'The :attribute must have alpha numeric dash value',
            'exists' => 'Doctor doesn\'t exist',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters(): array
    {
        return [
        ];
    }   
}
