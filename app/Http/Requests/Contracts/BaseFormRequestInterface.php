<?php

namespace App\Http\Requests\Contracts;


interface BaseFormRequestInterface
{
    public function authorize(): bool;
    public function rules(): array;
    
    public function extractValidationErrors(array $errors): string;
}
