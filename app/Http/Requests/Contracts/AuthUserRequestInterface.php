<?php

namespace App\Http\Requests\Contracts;


interface AuthUserRequestInterface
{
    public function authorize(): bool;
    public function rules(): array;
    public function messages(): array;
    public function filters(): array;
}
