<?php

namespace App\Http\Requests;

/**
* Validate bank custodian request
* During use signup
*/
use App\Http\Requests\Contracts\GeneralRequestInterface;

use Exception;

class UpdateDoctorBio extends BaseFormRequest implements GeneralRequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'career_stage'    => 'required|string|max:50',
            'specialty' => 'required|string|max:100',
            'subspecialty' => 'sometimes|required|string|max:100',
            'howner' => 'required|boolean',
            'folio_no' => 'sometimes|required|alpha_dash|max:20',
            //'foliopic' => 'sometimes|required|image',
            
            'workplace_type'    => 'required|alpha_dash|max:40',
            'workplace_name'    => 'required|string|max:100',
            'country' => 'sometimes|alpha|between:0,100',
            'job_desc' => 'sometimes|required|string|max:500',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'integer'      => 'The :attribute must have an integer value',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters(): array
    {
        return [
        ];
    }   
}
