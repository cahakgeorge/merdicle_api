<?php

namespace App\Http\Requests;

/**
* Validate bank custodian request
* During use signup
*/
use App\Http\Requests\Contracts\GeneralRequestInterface;

use Exception;

class CreatePostRequest extends BaseFormRequest implements GeneralRequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'post_text' => 'sometimes|required|string|between:0,1000',
            'post_type' => 'required|alpha_dash|between:1,20',
            'post_visib' => 'required|alpha_dash|between:1,15',
            'post_is_for' => 'sometimes|alpha_dash|between:1,12',

            'post_owner_id' => 'sometimes|required|integer',

            'img' => 'sometimes|required|image',
            'vid' => 'sometimes|required|file',
            'doc' => 'sometimes|required|file',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'integer'      => 'The :attribute must have an integer value',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters(): array
    {
        return [
        ];
    }   
}
