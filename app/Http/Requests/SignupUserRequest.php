<?php

namespace App\Http\Requests;

/**
* Validate bank custodian request
* During use signup
*/
use App\Http\Requests\Contracts\GeneralRequestInterface;

use Exception;

class SignupUserRequest extends BaseFormRequest implements GeneralRequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required_without:phone|unique:user,email|email|between:10,191',
            //'phone' => 'required_without:email|unique:users,phone|digits_between:10,14',
            'first_name' => 'bail|required|alpha_dash|max:100',
            'last_name' => 'bail|required|alpha_dash|max:100',
            'gender' => 'required|alpha',
            //'dial_code' => 'required_with:phone|numeric|exists:countries,dial_code|between:2,4',
            'password' => 'bail|required|string|between:4,100',

            'image' => 'required|image',
            'dob' => 'required|date',

            'nationality' => 'required|alpha|between:0,100',
            'mstatus' => 'required|alpha|between:5,40',
            'signup_type'=>'required|alpha|between:3,10',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'integer'      => 'The :attribute must have an integer value',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters(): array
    {
        return [
        ];
    }   
}
