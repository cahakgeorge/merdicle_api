<?php

namespace App\Http\Requests;

/**
* Validate bank custodian request
* During use signup
*/
use App\Http\Requests\Contracts\GeneralRequestInterface;

use Exception;

class CreateCaseRequest extends BaseFormRequest implements GeneralRequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'sometimes|required|string|between:0,100',
            'case_description' => 'required|string|between:1,400',
            'symptoms' => 'required|string|between:1,400',
            'diagnosis' => 'required|string|between:1,400',

            'complications' => 'required|string|between:1,400',

            'causes' => 'required|string|between:1,400',
            'treatment' => 'required|string|between:1,400',
            'image' => 'sometimes|required|image',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'integer'      => 'The :attribute must have an integer value',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters(): array
    {
        return [
        ];
    }   
}
