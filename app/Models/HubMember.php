<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HubMember extends Model
{
    protected $table = 'hub_member';
    protected $primaryKey = 'id';
    
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public  $timestamps = false;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Mass assignable attributes
     *
     * @var array
     */
    protected $fillable = [
         'hub_id', 'user_id', 'status', 'created_at'
    ];

    /**
     * Hidden attributes
     *
     * @var array
     */
    protected $hidden = [
    ];

}
