<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $table = 'appointment';
    protected $primaryKey = 'id';
    
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public  $timestamps = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Mass assignable attributes
     *
     * @var array
     */
    protected $fillable = [
         'user_id', 'doctor_id', 'title', 'description', 'date', 'location', 'status', 'edit_log','created_at'
    ];

    /**
     * Hidden attributes
     *
     * @var array
     */
    protected $hidden = [
    ];

}
