<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ForgotPasswordToken extends Model
{
    protected $table = 'forgot_password_token';
    protected $primaryKey = 'id';
    
    public  $timestamps = false; 
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'user_id', 'search_input', 'type', 'token', 'active', 'created_at'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'email', 'email');
    }
}
