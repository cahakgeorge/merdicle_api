<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
    protected $table = 'hospital';
    protected $primaryKey = 'id';
    
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public  $timestamps = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Mass assignable attributes
     *
     * @var array
     */
    protected $fillable = [
        'name', 'owner', 'address', 'email', 'phone', 'staff_strength', 'bio', 'latlng'
        , 'staff_strength', 'consultant_count', 'specialist_count', 'type', 'recieve_appoints', 'services', 'created_at'
    ];

    /**
     * Hidden attributes
     *
     * @var array
     */
    protected $hidden = [
    ];

}
