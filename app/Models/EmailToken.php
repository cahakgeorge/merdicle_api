<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailToken extends Model
{
    protected $table = 'email_token';
    protected $primaryKey = 'id';
    
    public  $timestamps = true; //updated_at and created_at columns available, Laravel is free to maintain date columns
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'email', 'token', 'active', 'created_at'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
