<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CaseRating extends Model
{
    protected $table = 'case_rating';
    protected $primaryKey = 'id';
    
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public  $timestamps = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Mass assignable attributes
     *
     * @var array
     */
    protected $fillable = [
         'case_id', 'user_id', 'rating', 'created_at'
    ];

    /**
     * Hidden attributes
     *
     * @var array
     */
    protected $hidden = [
    ];

}
