<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cases extends Model
{
    protected $table = 'cases';
    protected $primaryKey = 'id';
    
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public  $timestamps = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Mass assignable attributes
     * 
     * @var array
     */
    protected $fillable = [
         'title', 'creator_id', 'case_description', 'symptoms', 'diagnosis', 'complications', 'causes',
        'treatment', 'has_extra', 'extra_url', 'created_at'
    ];

    /**
     * Hidden attributes
     *
     * @var array
     */
    protected $hidden = [
    ];

}
