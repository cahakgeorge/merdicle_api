<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneToken extends Model
{
    protected $table = 'phone_token';
    protected $primaryKey = 'id';
    
    public  $timestamps = true; //updated_at and created_at columns available, Laravel is free to maintain date columns
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'phone', 'token', 'active'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
