<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles, AuthenticableTrait, SoftDeletes; //, SoftDeletes

    protected $guard_name = 'api';

    protected $table = 'user';
    protected $primaryKey = 'id';
    
    protected $date = [
        'created_at', 'updated_at', 'deleted_at',
        'email_verified_at'
    ];

   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'first_name', 'last_name', 'phone', 'dial_code', 'password', 'bio', 
        'user_type', 'user_pic_url', 'email_verified_at', 'created_at', 'status', 'gender', 
        'date_of_birth', 'marital_status', 'nationality', 'location', 'bloodgroup',
        'genotype', 'med_history', 'edu_level', 'grad_year', 'folio_num',
        'career_stage', 'specialty', 'sub_specialty', 'licence_img_id', 'run_hospital', 'med_school'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    public function settings()
    {
        return $this->hasMany('App\Models\Setting');
    }

    public function findForPassport($identifier) {
        return $this->orWhere('email', $identifier)->orWhere('phone', $identifier)->first();
    }
}
