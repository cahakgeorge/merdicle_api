<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Hash;

use App\Models\{ User, PhoneToken, EmailToken, ForgotPasswordToken, Practice, Association, Post, Hub,
                    Hospital, PostLike, PostBookmark, Notification, Follow, BlockedUser, Cases, CaseRating, 
                    HubMember, AssociationMember, Appointment
};


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use App\Repositories\Contracts\UserRepositoryInterface;

use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * This class handles all calls, queries to the user model
 *
 * @var array
 */
class UserRepository implements UserRepositoryInterface
{
    protected $user;
    protected $phoneToken;
    protected $emailToken;
    protected $forgotPasswordToken;

    protected $practice;
    protected $association;
    protected $post;
    protected $hub;
    protected $hospital;

    protected $postLike;
    protected $postBookmark;
    protected $notification;
    protected $follow;
    protected $blockedUser;
    protected $cases;
    protected $caseRating;
    protected $hubMember;

    protected $associationMember;
    protected $appointment;
    
    
    public function __construct(User $user, PhoneToken $phoneToken, EmailToken $emailToken,
                                ForgotPasswordToken $forgotPasswordToken, Association $association, 
                                Post $post, Hub $hub, Hospital $hospital, Practice $practice, 
                                PostLike $postLike, PostBookmark $postBookmark, Notification $notification,
                                Follow $follow, BlockedUser $blockedUser, Cases $cases, CaseRating $caseRating, 
                                HubMember $hubMember, AssociationMember $associationMember, Appointment $appointment)
	{
        $this->user = $user;
        $this->phoneToken = $phoneToken;
        $this->emailToken = $emailToken;
        $this->forgotPasswordToken = $forgotPasswordToken;

        $this->practice = $practice;
        $this->association = $association;
        $this->post = $post;
        $this->hub = $hub;
        $this->hospital = $hospital;
        $this->postLike = $postLike;
        $this->postBookmark = $postBookmark;
        $this->notification = $notification;

        $this->follow = $follow;
        $this->blockedUser = $blockedUser;
        $this->cases = $cases;
        $this->caseRating = $caseRating;
        $this->hubMember = $hubMember;
        $this->associationMember = $associationMember;
        $this->appointment = $appointment;
	}

    public function createRecord(array $input)
	{
		return $this->user->create($input);
    }

    public function findFirstById($id)
	{
		return $this->user->where('id', $id)->first();
    }


    /**
    * Save email token for user
    * @return bool
    */
    public function createEmailToken(string $token, string $email)
	{
        $time = Carbon::now(); 
        
		DB::beginTransaction();

		try{
            //update all previous tokens to inactive
            DB::update("UPDATE email_token SET active='false' WHERE email='$email' ");
                                        
			//persist token
			$createdTokenRecord = $this->emailToken->create(['email'=>$email,
                                                                'token'=>$token, 'active'=>'true']);

			DB::commit();
			
			return $createdTokenRecord;
		
		}catch(Exception $e){
			DB::rollback();

			throw new HttpResponseException(response()->json(["status"=>false, "data"=>null, "message"=>"Failed to create user token"], 200));
		}
    }
    



    /**
    * Verify email token, then create account
    * @return bool
    */
    public function verifyEmailTokenThenCreateAccount($request, string $profilePic="")
	{
        $time = Carbon::now(); 
        $userCreated = null;

		DB::beginTransaction();

		try{
            
            //find email token first
            $emailTokenData = $this->emailToken->where('token', $request->token)
                                                    ->where('email', $request->email)
                                                    ->whereBetween('created_at',[Carbon::now()->subMinutes(60), Carbon::now()])->first();

            if($emailTokenData){//now create user account
                $userCreated = $this->createRecord([
                    'email' => $request->email, 'first_name' => $request->first_name, 'last_name' => $request->last_name, 'gender' => $request->gender,
                    'password' => bcrypt($request->password), 'date_of_birth' => $request->dob, 'marital_status' => $request->mstatus, 'nationality' => $request->nationality, 
                    'user_type'=>$request->signup_type, 'email_verified_at' => $time,
                    'user_pic_url'=>$profilePic, 'status'=>'active'
                ]);
                
                $this->assignUserRole($userCreated, $request->signup_type);

                $emailTokenData->update(['active'=>'false', 'updated_at'=>$time]);
            }

			DB::commit();
			
			return $userCreated;
            
		}catch(Exception $e){
			DB::rollback();

			throw new HttpResponseException(response()->json(["status"=>false, "data"=>null, "message"=>"Failed to verify token"], 200));
        }
	}

    
    /**
    * Save forgot password token for user
    * @return bool
    */
    public function createForgotPasswordToken(string $searchEmail, string $token)
	{
        $time = Carbon::now();
        
		DB::beginTransaction();

		try{
            //check if user exists
            $user = $this->user->where('email', $searchEmail)->first();

            if($user){
                //update all previous tokens for this user to inactive
                DB::update("UPDATE forgot_password_token SET active='2' WHERE search_input='$searchEmail' ");
                                        
                //persist fpassword token to db
                $createdTokenRecord = $this->forgotPasswordToken->create(['search_input'=>$searchEmail, 'user_id'=>$user->id,'token'=>$token, 
                    'type'=>'email', 'active'=>'true', 'created_at'=>$time]);
            
            }

			DB::commit();
			
			return [$user, $createdTokenRecord];
		
		}catch(Exception $e){
			DB::rollback();

			throw new HttpResponseException(response()->json(["status"=>false, "data"=>null, "message"=>"Failed to create user token"], 200));
		}
    }

    /**
    * Verify forgot password token, then update account
    * @return bool
    */
    public function verifyForgotPasswordTokenThenUpdatePassword($request)
	{
        $time = Carbon::now(); 
        
		DB::beginTransaction();
        
		try{
            
            //confirm token and email first
            $tokenData = $this->forgotPasswordToken->where('token', $request->token)
                                                    ->where('search_input', $request->email)
                                                    ->whereBetween('created_at',[Carbon::now()->subMinutes(30), Carbon::now()])->first();

            if($tokenData){//now update user passsword
                User::where('id', $tokenData->user_id)->update(['password' => bcrypt($request->password)]);

                $tokenData->update(['active'=>'false']);
            }

			DB::commit();
			
			return $tokenData;
		
		}catch(Exception $e){
			DB::rollback();

			throw new HttpResponseException(response()->json(["status"=>false, "data"=>null, "message"=>"Failed to verify token"], 200));
        }
    }



    /**
    * update password from profile
    * @return bool
    */
    public function updatePasswordFromProfile(object $user, object $request)
	{
        $time = Carbon::now(); 
        
        //update password after comparison
        if (Hash::check($request->old, $user->password)) {//passwords match
            return $user->update(['password'=>bcrypt($request->new), 'updated_at'=>$time] );
        }

        return false;
    }


    /**
     * Get access token for user
     */
    public function getAccessTokenForUser($user): string{
        //get token
        $tokenResult = $this->createUserAccessToken($user);
        if(!$tokenResult) throw new HttpResponseException(response()->json(["status"=>false,"data"=>null,"message"=>"No created token"], 200));

        $token = $tokenResult->token;
        
        $token->save();

        return $tokenResult->accessToken; //could also retreve the refresh token for the access token generated here as well
    }

    /**
    * Create passport token for user
    * @return string
    */
    public function createUserAccessToken($user): object{
        //retrieve passport token name from environment, then create token for user
        $passportAppName = env('PASSPORT_TOKEN_NAME', 'default');

        return $user->createToken($passportAppName); 
    }
    
    /**
    * Assign normal user role
    * @return bool
    */
    public function assignUserRole(object $user, string $type){
        return $user->assignRole(strtolower($type));
    }
    
    /**
    * Update patient bio
    * @return bool
    */
    public function updatePatientBio(object $user, object $request)
	{
        $time = Carbon::now();
        
        return $user->update(['bloodgroup'=>$request->blood_group, 'genotype'=>$request->genotype, 
                            'edu_level'=>$request->edu_level ?? '', 'med_history'=>$request->mhistory ?? '', 
                            'location'=>$request->location, 'updated_at'=>$time] );
    }
    
    /**
    * Update doctor bio
    * @return bool
    */
    public function updateDoctorBio(object $user, object $request)
	{       
        $time = Carbon::now(); 

		DB::beginTransaction();

		try{    
            $user->update(['career_stage'=>$request->career_stage, 'specialty'=>$request->specialty, 
                            'sub_specialty'=>$request->subspecialty ?? '',  'run_hospital'=>$request->howner, 
                            'folio_num'=>$request->folio_no, 'updated_at'=>$time]);//'licence_img_id'=>$request->mhistory,
            
            $this->practice->create(['user_id'=>$user->id, 'name'=>$request->workplace_name, 'job_desc'=>$request->job_desc ?? '',
                                       'type'=>$request->workplace_type, 'country'=>$request->country ?? '', 'created_at'=>$time]);
            DB::commit();
			
			return true;
		}catch(Exception $e){
			DB::rollback();
            
			throw new HttpResponseException(response()->json(["status"=>false, "data"=>null, "message"=>"Failed to update doc bio"], 200));
        }   
    }       
    
    public function updateUserProfile(object $user, $request)
	{
        return $user->update(['first_name'=>$request->first_name, 'last_name'=>$request->last_name]);
    }

    /**
    * Get profile for user with id $userId
    * @return bool
    */
    public function getUserProfile(int $userId, int $myUserId)
	{
        return $this->user->selectRaw("id, email, first_name, last_name, user_type, bio, user_pic_url, career_stage,
                                 run_hospital, folio_num, specialty, sub_specialty, location, date_of_birth, gender, 
                                 
                                 (SELECT CASE
                                        WHEN (SELECT count(id) FROM follow WHERE user_id='$myUserId' AND follows_user_id='$userId') > 0 THEN 'yes'
                                        ELSE 'no'
                                        END as do_i_follow) AS do_i_follow,

                                 (SELECT count(DISTINCT user_id) FROM follow WHERE follows_user_id='$userId') as followers_count,
                                 (SELECT count(DISTINCT follows_user_id) FROM follow WHERE user_id='$userId') as ifollow_count")
                                ->where('id', $userId)->first();
    }

    /**
    * Get user profile
    * @return bool
    */
    /* public function getUserProfile(object $userId)
	{
        return $this->user->selectRaw("id, email, first_name, last_name, user_type, bio, user_pic_url, career_stage,
                                 run_hospital, folio_num, specialty, sub_specialty, location, date_of_birth, gender, 
                                 
                                 (SELECT count(DISTINCT user_id) FROM follow WHERE follows_user_id='$userId') as followers_count,
                                 (SELECT count(DISTINCT follows_user_id) FROM follow WHERE user_id='$userId') as ifollow_count, 
                                 SELECT count(uid) FROM ( (SELECT follows_user_id AS uid FROM follow WHERE user_id='userId') INTERSECT
                                    (SELECT user_id AS uid  FROM follow WHERE follows_user_id='$userId') ) as connections")
                                ->where('id', $userId)->first();

                                //select all my followers, then select all those i follow
                                //finally look for intersection, in count
    } */



    public function deleteAccount($user)
	{
		return $user->delete();
    }

    /**
    * Create user post
    * @return bool
    */
    public function createPost(array $postdataArray)
	{
        return $this->post->create($postdataArray);
    }

    /**
    * Get posts for user
    * @return bool
    */
    public function getPosts(int $userId)
	{
        return $this->post->selectRaw("post.id, user_id, post_type, visibility, has_extra, content,
                                        post_is_for, owner_id, extra_url, user.created_at,  user.user_pic_url, 
                                        user.first_name, user.last_name, user.user_type, user.career_stage,

                                        (SELECT CASE
                                            WHEN (SELECT count(id) FROM post_like WHERE user_id='$userId' AND post_id=post.id) > 0 THEN 'yes'
                                            ELSE 'no'
                                            END as i_like) AS i_like,
                                        (SELECT CASE
                                            WHEN (SELECT count(id) FROM post_bookmark WHERE user_id='$userId' AND post_id=post.id) > 0 THEN 'yes'
                                            ELSE 'no'
                                            END as is_bookmarked) AS is_bookmarked,

                                        (SELECT CASE
                                            WHEN (SELECT count(id) FROM post WHERE user_id='$userId' AND post_is_for='share' AND owner_id=post.id) > 0 THEN 'yes'
                                            ELSE 'no'
                                            END as i_shared) AS i_shared,

                                        (SELECT count(id) FROM post_like WHERE post_id=post.id) as like_count,
                                        (SELECT count(id) FROM post AS post2 WHERE post2.post_is_for='share' AND post2.owner_id=post.id) as share_count,
                                        (SELECT count(id) FROM post AS post3 WHERE post3.post_is_for='comment' AND post3.owner_id=post.id) as comment_count")
                    ->join('user', 'post.user_id', 'user.id')
                    ->orderBy('post.created_at', 'desc')->get();
    }

    /**
    * Get posts for single user
    * @return bool
    */
    public function getAllPostsForUser(int $userId)
	{
        return $this->post->selectRaw("id, user_id, post_type, visibility, has_extra, content,
                                        post_is_for, owner_id, extra_url, created_at,

                                        (SELECT count(id) FROM post_like WHERE post_id=post.id) as like_count,
                                        (SELECT count(id) FROM post AS post2 WHERE post2.post_is_for='share' AND post2.owner_id=post.id) as share_count,
                                        (SELECT count(id) FROM post AS post3 WHERE post3.post_is_for='comment' AND post3.owner_id=post.id) as comment_count")

                    ->where('post_is_for', '!=', 'assoc')->where('post_is_for', '!=', 'hub')
                    ->orderBy('created_at', 'desc')
                    ->get();

                    //TOD, responses or comments on such hub posts will still be shownn,
                    //create filter to hide such posts as well when showing all posts for this specific user
    }

    /**
    * Get posts made by a specific user
    * @return bool
    */
    public function getSpecificUserPost(int $userId)
	{
        return $this->post->selectRaw("id, user_id, post_type, visibility, has_extra, content,
                                        post_is_for, owner_id, extra_url, created_at,

                                        (SELECT count(id) FROM post_like WHERE post_id=post.id) as like_count,
                                        (SELECT count(id) FROM post AS post2 WHERE post2.post_is_for='share' AND post2.owner_id=post.id) as share_count,
                                        (SELECT count(id) FROM post AS post3 WHERE post3.post_is_for='comment' AND post3.owner_id=post.id) as comment_count")
                            ->where('user_id', $userId)
                            ->orderBy('created_at', 'desc')
                            ->get();
    }

    /**
    * Like user post
    * @return bool
    */
    public function likePost(int $userId, int $postId)
	{
        $postAlreadyLiked = $this->postLike->where('user_id', $userId)->where('post_id', $postId)->first();
        if($postAlreadyLiked) return true;
        
        return $this->postLike->create(['user_id'=>$userId, 'post_id'=>$postId, 'created_at'=>Carbon::now()]);
    }

    /**
    * Unlike post a user previously liked
    * @return bool
    */
    public function unlikePost(int $userId, int $postId)
	{
        return $this->postLike->where('user_id', $userId)->where('post_id', $postId)->delete();
    }


    /**
    * Get post comment
    * @return bool
    */
    public function getPostComment(int $postId)
	{
        return $this->post->selectRaw("id, user_id, post_type, visibility, has_extra, content,
                                        post_is_for, owner_id, extra_url, created_at,

                                        (SELECT count(id) FROM post_like WHERE post_id=post.id) as like_count,
                                        (SELECT count(id) FROM post AS post2 WHERE post2.post_is_for='comment' AND post2.owner_id=post.id) as comment_count")

                            ->where('post_is_for', 'comment')->where('owner_id', $postId)
                    ->get();
    }

    /**
    * Delete post comment
    * @return bool
    */
    public function deletePostComment(int $userId, int $postId)
	{
        //print_r(func_get_args());
        return $this->post->where('id', $postId)->where('user_id', $userId)->where('post_is_for', 'comment')->delete();
    }

    /**
    * Get all bookmarks
    * @return bool
    */
    public function getBookmarkedPosts(int $userId)
	{   
        return $this->postBookmark->where('post.user_id', $userId)
                ->selectRaw("post.id, post.user_id, post_type, visibility, has_extra, content,
                post_is_for, owner_id, extra_url, post.created_at,

                (SELECT count(id) FROM post_like WHERE post_id=post.id) as like_count,
                                        (SELECT count(id) FROM post AS post2 WHERE post2.post_is_for='share' AND post2.owner_id=post.id) as share_count,
                                        (SELECT count(id) FROM post AS post3 WHERE post3.post_is_for='comment' AND post3.owner_id=post.id) as comment_count")
                ->join('post', 'post_bookmark.post_id', 'post.id')->get();
    }
    
    /**
    * Bookmark a post
    * @return bool
    */
    public function createPostBookmark(int $userId, int $postId)
	{
        $postAlreadyBookmarked = $this->postBookmark->where('user_id', $userId)->where('post_id', $postId)->first();
        if($postAlreadyBookmarked) return true;

        return $this->postBookmark->create(['user_id'=>$userId, 'post_id'=>$postId, 'created_at'=>Carbon::now()]);
    }

    /**
    * Delete post bookmark
    * @return bool
    */
    public function deletePostBookmark(int $userId, int $postId)
	{
        return $this->postBookmark->where('user_id', $userId)->where('post_id', $postId)->delete();
    }

    /**
    * Get users who shared post
    * @return bool
    */
    public function getUsersWhoSharedPost(int $postId)
	{   
        $sharersIdArray = $this->post->where('post_is_for', 'share')->where('owner_id', $postId)->select('user_id')->get()->toArray();

        return $this->user->whereIn('id', $sharersIdArray)
                        ->selectRaw("id, email, first_name, last_name, user_type, bio, user_pic_url, career_stage,
                                    run_hospital, folio_num, specialty, sub_specialty, location, date_of_birth, gender")
                        ->get();
    }

    /**
    * Get user notifications for user with id @int $userId
    * @return bool
    */
    public function getUserNotifications(int $userId)
	{
        return $this->notification->where('for_who', $userId)->select('for_who', 'type', 'message', 'placeholder_values', 'created_at', 'pic_url', DB::raw("'yes' as i_follow"))->get();
    }
    
    /**
    * Follow user
    * @return bool
    */
    public function followUser(int $userId, int $userToFollow)
	{
        $userAlreadyFollowed = $this->follow->where('user_id', $userId)->where('follows_user_id', $userToFollow)->first();
        if($userAlreadyFollowed) return true;

        return $this->follow->create(['user_id'=>$userId, 'follows_user_id'=>$userToFollow, 'created_at'=>Carbon::now()]);
    }

    /**
    * unfollow user
    * @return bool
    */
    public function unfollowUser(int $userId, int $userToUnfollow)
	{
        return $this->follow->where('user_id', $userId)->where('follows_user_id', $userToUnfollow)->delete();
    }

    /**
    * Block user
    * @return bool
    */
    public function blockUser(int $userId, int $userToBlock, string $reason=null)
	{
        return $this->blockedUser->create(['user_id'=>$userId, 'blocked_id'=>$userToBlock, 'report'=>$reason, 'created_at'=>Carbon::now()]);
    }
    
    /**
    * Get case review
    * @return bool
    */
    public function getCaseReviews(int $caseId)
	{
        return $this->post->selectRaw("id, user_id, post_type, visibility, has_extra, content,
                                        post_is_for, owner_id, extra_url, created_at")

                            ->where('post_is_for', 'case_review')->where('owner_id', $caseId)
                            ->get();
    }

    /**
    * Create case
    * @return bool
    */
    public function createCase(array $caseArrayData)
	{
        return $this->cases->create($caseArrayData);
    }
    
    /**
    * Create case rating
    * @return bool
    */
    public function createCaseRating(array $caseRatingDataArray)
	{
        return $this->caseRating->create($caseRatingDataArray);
    }   

    /**
    * Get cases
    * @return bool
    */
    public function getCases()
	{
        return $this->cases->selectRaw("cases.id, creator_id, title, case_description, symptoms, diagnosis,
                                        complications, causes, treatment, has_extra,
                                        extra_url, cases.created_at,
                                        user.first_name, user.last_name, user.user_pic_url,
                                        (SELECT count(id) FROM post WHERE post_is_for='case_review' AND owner_id=cases.id) as review_count,
                                        (SELECT avg(rating) FROM case_rating WHERE case_id=cases.id) as rating
                                        ")
                            ->join('user', 'cases.creator_id', 'user.id')->get();
    }


    /**
    * Get connection screen data
    * @return bool
    */
    public function getMyConnections(int $userId)
	{
        $connectionData = DB::select("SELECT user.id, email, first_name, last_name, user_type, bio, user_pic_url, career_stage,
                                 run_hospital, folio_num, specialty, sub_specialty, location, date_of_birth, gender,
                                 'yes' AS follows_me,
                                 (SELECT CASE
                                        WHEN (SELECT count(id) FROM follow WHERE user_id='$userId' AND follows_user_id=user.id) > 0 THEN 'yes'
                                        ELSE 'no'
                                        END as do_i_follow) AS do_i_follow 
                                 FROM user WHERE user.id IN (SELECT user_id AS id FROM follow WHERE follows_user_id='$userId') 
                                 
                                UNION 

                                SELECT id, email, first_name, last_name, user_type, bio, user_pic_url, career_stage,
                                                run_hospital, folio_num, specialty, sub_specialty, location, date_of_birth, gender,
                                                'yes' AS do_i_follow,
                                                (SELECT CASE
                                                        WHEN (SELECT count(id) FROM follow WHERE follows_user_id='$userId' AND user_id=user.id) > 0 THEN 'yes'
                                                        ELSE 'no'
                                                        END as follows_me) AS follows_me 
                                                FROM user WHERE user.id IN (SELECT follows_user_id AS id FROM follow WHERE user_id='$userId')     
                            ");

        $suggestionData = DB::select("SELECT user.id, email, first_name, last_name, user_type, bio, user_pic_url, career_stage,
                                 run_hospital, folio_num, specialty, sub_specialty, location, date_of_birth, gender

                                 FROM user WHERE user.id NOT IN (SELECT follows_user_id AS id FROM follow WHERE user_id='$userId') AND user.id != '$userId'
                                 
                            ");//where user id is not in list of people i follow  AND user_type='doctor' 
        
        $myFollowerCount = $this->follow->where('follows_user_id', $userId)->count();
        $iFollowCount = $this->follow->where('user_id', $userId)->count();

        return ['follower_count'=>$myFollowerCount, 'ifollow_count'=>$iFollowCount, 'connection'=>$connectionData, 'suggestion'=>$suggestionData];
    }
    

    /**
    * Join association
    * @return bool
    */
    public function joinAssociation(int $userId, int $assocId)
	{
        //get association
        $association = $this->association->find($assocId);
        
        if($association){
            $associationAlreadyJoined = $this->associationMember->where('user_id', $userId)->where('association_id', $assocId)->first();
            if($associationAlreadyJoined) return true;

            return $this->associationMember->create(['user_id'=>$userId, 'association_id'=>$assocId, 'created_at'=>Carbon::now(), 'status'=>($association->free_entry=='true') ? 'open' : 'pending' ]);
        }
        
        return false;
    }

    /**
    * Leave association
    * @return bool
    */
    public function leaveAssociation(int $userId, int $assocId)
	{
        return $this->associationMember->where('user_id', $userId)->where('association_id', $assocId)->delete();
    }
    
    /**
    * Join hub
    * @return bool
    */
    public function joinHub(int $userId, int $hubId)
	{
        $hubAlreadyJoined = $this->hubMember->where('user_id', $userId)->where('hub_id', $hubId)->first();
        if($hubAlreadyJoined) return true;

        return $this->hubMember->create(['user_id'=>$userId, 'hub_id'=>$hubId, 'created_at'=>Carbon::now(), 'status'=>'pending' ]);
    }

    /**
    * Leave hub
    * @return bool
    */
    public function leaveHub(int $userId, int $hubId)
	{
        return $this->hubMember->where('user_id', $userId)->where('hub_id', $hubId)->delete();
    }

    /**
    * Create appointment
    * @return bool
    */
    public function createPatientAppointment(array $appointmentDataArray)
	{
        return $this->appointment->create($appointmentDataArray);
    }
    
    /**
    * Get user appointments
    * @return bool
    */
    public function getUserAppointments(int $userId)
	{
        return $this->appointment->selectRaw("appointment.id, doctor_id, user.first_name, user.last_name, user.user_pic_url, title, date, appointment.location,
                                                appointment.status, edit_log, appointment.created_at
                                        ")
                            ->join('user', 'appointment.doctor_id', 'user.id')->where('user_id', $userId)->get();
    }
    

    /**
    * Approve appointment
    * @return bool
    */
    public function approveAppointment(int $userId, int $appointmentId)
	{
        $appointment = $this->appointment->where('id', $appointmentId)->where('doctor_id', $userId)->orWhere('user_id', $userId)->first();
        $editLog = explode('#*#', $appointment);
        $lastEditLog = explode('|', $editLog[sizeof($editLog)-1]);

        //for example, patient pushes last appointment update, doctor accepts
        //or doctor pushes last appointment update, patient accepts, different ids from previous
        if($lastEditLog[1] != $userId) return $appointment->update(['status'=>'accepted', 'updated_at'=>Carbon::now()]);

        return false;
    }

    /**
    * Get appointment
    * @return bool
    */
    public function getAppointment(int $userId, int $appointmentId)
	{
        return $this->appointment->where('id', $appointmentId)->where('doctor_id', $userId)
                                    ->orWhere('user_id', $userId)
                                    ->select('id', 'doctor_id', 'user_id', 'location', 'date', 'status',  'edit_log')->first();
    }

    /**
    * Update appointment
    * @return bool
    */
    public function editAppointment(object $appointment, array $appointmentupdataArray)
	{
        return $appointment->update($appointmentupdataArray);
    }
}