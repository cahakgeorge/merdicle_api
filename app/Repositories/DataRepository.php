<?php

namespace App\Repositories;

use App\Models\Country;
use App\Models\Specialty;
use App\Models\Service;

use App\Models\Association;
use App\Models\Post;
use App\Models\User;

use App\Models\Hub;
use App\Models\Hospital;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use App\Repositories\Contracts\DataRepositoryInterface;

use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * This class handles all calls, queries to retrieve data from models
 *
 * @var array
 */
class DataRepository implements DataRepositoryInterface
{

    protected $country;
    protected $specialty;
    protected $hservice;

    protected $association;
    protected $post;
    protected $user;
    protected $hub;
    protected $hospital;

    public function __construct(Country $country, Specialty $specialty, Service $hservice,
                                    Association $association, Post $post, User $user, Hub $hub, Hospital $hospital)
	{
        $this->country = $country;
        $this->specialty = $specialty;
        $this->hservice = $hservice;

        $this->association = $association;
        $this->post = $post;
        $this->user = $user;

        $this->hub = $hub;
        $this->hospital = $hospital;
	}

    /**
    * Get onboard data
    * @return bool
    */
    public function getOnboardData()
	{       
        $time = Carbon::now(); 

        $countries = $this->country->where('active', '1')->select('id', 'country', 'dial_code')->get();

        $specialties = $this->specialty->select('id', 'name', 'detail', 'subs', 'type')->get();

        $hservices = $this->hservice->select('id', 'name', 'detail')->get();
        
        return ['country'=>$countries, 'specialty'=>$specialties, 'service'=>$hservices];
    }    
    
    /**
    * Create association
    * @return bool
    */
    public function createAssociation(array $associationDataArray)
	{
        return $this->association->create($associationDataArray);
    }

    /**
    * Get associations list
    * @return bool
    */
    public function getAssociations(int $userId)
	{
        return $this->association->selectRaw("association.id, name, admins, association.bio, pic_url, free_entry,
                                        association.created_at, created_by,
                                        (SELECT count(user_id) FROM association_member WHERE association_id=association.id) as member_count,
                                        (SELECT CASE
                                                        WHEN (SELECT count(id) FROM association_member WHERE association_id=association.id AND user_id='$userId') > 0 THEN 'yes'
                                                        ELSE 'no'
                                                        END as i_am_member) AS i_am_member,
            
                                                        user.first_name, user.last_name
                                        ")
                                        ->join('user', 'association.created_by', 'user.id')
                                        ->groupBy('association.id')->get(); //i_am_member should also check if users membership is approved i.e AND status='open' or 'approved'
    }

    /**
    * Get posts targeted at specific association
    * @return bool
    */
    public function getAssociationPosts(int $assocationId)
	{
        return $this->post->selectRaw("id, user_id, post_type, visibility, has_extra, content,
                                        post_is_for, owner_id, extra_url, created_at,

                                        (SELECT count(id) FROM post_like WHERE post_id=post.id) as like_count,
                                        (SELECT count(id) FROM post AS post2 WHERE post2.post_is_for='share' AND post2.owner_id=post.id) as share_count,
                                        (SELECT count(id) FROM post AS post3 WHERE post3.post_is_for='comment' AND post3.owner_id=post.id) as comment_count")
                            ->where('post_is_for', 'assoc')->where('owner_id', $assocationId)
                            ->orderBy('created_at', 'desc')
                            ->get();
    }
    
    /**
    * Get association members
    * @return bool
    */
    public function getAssociationMembers(int $associationId)
	{   
        return $this->user->selectRaw("id, email, first_name, last_name, user_type, bio, user_pic_url, career_stage,
                                 run_hospital, folio_num, specialty, sub_specialty, location, date_of_birth, gender")
                                ->whereIn('id', function($query) use($associationId){
                                        $query->select('user_id')->from('association_member')->where('association_id', $associationId);
                                    })->get();
    }   
    
    /**
    * Create hub
    * @return bool
    */
    public function createHub(array $hubDataArray)
	{
        return $this->hub->create($hubDataArray);
    }

    /**
    * Get hub list
    * @return bool
    */
    public function getHubs(int $userId)
	{
        return $this->hub->selectRaw("id, name, pic_url, creator_id, created_at,
                                        (SELECT count(user_id) FROM hub_member WHERE hub_id=hub.id) as member_count,
                                        (SELECT CASE
                                                        WHEN (SELECT count(id) FROM hub_member WHERE hub_id=hub.id AND user_id='$userId') > 0 THEN 'yes'
                                                        ELSE 'no'
                                                        END as i_am_member) AS i_am_member,
                                        (SELECT count(DISTINCT id) FROM post WHERE post_is_for='hub' AND owner_id=hub.id) as discussion_count,
                                        (SELECT count(DISTINCT id) FROM post WHERE parent_post_is_for='hub' AND owner_id=hub.id) as reply_count
                                        ")->groupBy('id')->get(); //i_am_member should also check if users membership is approved i.e AND status='open' or 'approved'
    }

    /**
    * Get posts targeted at specific hub
    * @return bool
    */
    public function getHubPosts(int $hubId)
	{
        return $this->post->selectRaw("id, user_id, post_type, visibility, has_extra, content,
                                        post_is_for, owner_id, extra_url, created_at,

                                        (SELECT count(id) FROM post_like WHERE post_id=post.id) as like_count,
                                        (SELECT count(id) FROM post AS post2 WHERE post2.post_is_for='share' AND post2.owner_id=post.id) as share_count,
                                        (SELECT count(id) FROM post AS post3 WHERE post3.post_is_for='comment' AND post3.owner_id=post.id) as comment_count")
                            ->where('post_is_for', 'hub')->where('owner_id', $hubId)
                            ->orderBy('created_at', 'desc')
                            ->get();
    }

    /**
    * Get hub members
    * @return bool
    */
    public function getHubMembers(int $hubId)
	{   
        return $this->user->selectRaw("id, email, first_name, last_name, user_type, bio, user_pic_url, career_stage,
                                 run_hospital, folio_num, specialty, sub_specialty, location, date_of_birth, gender")
                                ->whereIn('id', function($query) use($hubId){
                                        $query->select('user_id')->from('hub_member')->where('hub_id', $hubId);
                                    })->get();
    }   

    /**
    * Create hospital
    * @return bool
    */
    public function createHospital(array $hospitalDataArray)
	{
        return $this->hospital->create($hospitalDataArray);
    }

    /**
    * Get posts targeted at specific hospital
    * @return bool
    */  
    public function getHospitalReview(int $hospitalId)
	{   
        return $this->post->selectRaw("id, user_id, post_type, visibility, has_extra, content,
                                        post_is_for, owner_id, extra_url, created_at,

                                        (SELECT count(id) FROM post_like WHERE post_id=post.id) as like_count,
                                        (SELECT count(id) FROM post AS post2 WHERE post2.post_is_for='share' AND post2.owner_id=post.id) as share_count,
                                        (SELECT count(id) FROM post AS post3 WHERE post3.post_is_for='comment' AND post3.owner_id=post.id) as comment_count")
                            ->where('post_is_for', 'hosp_review')->where('owner_id', $hospitalId)
                            ->orderBy('created_at', 'desc')
                            ->get();
    }

    /**
    * Get hospital list
    * @return bool
    */
    public function getHospitals()
	{
        return $this->hospital->selectRaw("id, name, owner, address, email, phone, staff_strength, bio, latlng, type, recieve_appoints, created_at,
                                            (SELECT avg(rating) FROM hospital_rating WHERE hospital_id=hospital.id) as rating
                                        ")->whereNull('deleted_at')->get();
    }
    
    /**
    * Get list of hospitals which match keyword
    * @return bool
    */
    public function getHospitalsViaAddress(array $keywords)
	{
        return $this->hospital->selectRaw("id, name, owner, address, email, phone, staff_strength, bio, latlng, type, recieve_appoints, created_at,
                                            (SELECT avg(rating) FROM hospital_rating WHERE hospital_id=hospital.id) as rating
                                        ")->whereNull('deleted_at')
                                        ->where(function ($query) use ($keywords) {
                                            foreach($keywords as $search) {
                                              $query->orWhere('name', 'like', '%' . $search . '%');
                                              $query->orWhere('address', 'like', '%' . $search . '%');
                                            }
                                          })->get(); //->paginate(3)
                                          //where queries could also be extended to accomodate for extra column searches
    }

    
}