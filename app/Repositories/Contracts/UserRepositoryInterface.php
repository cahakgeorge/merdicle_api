<?php

namespace App\Repositories\Contracts;


interface UserRepositoryInterface
{
    public function createRecord(array $input);
    public function createUserAccessToken($createdUser): object;
   
}
