<?php

namespace App\Helpers;

/**
 * @group Application settings and Data
 *
 * APIs for retrieving application settings and 
 * handling application states
 */

use Symfony\Component\HttpFoundation\Response;

use App\Helpers\Contracts\EmptyHelperInterface;

//use NetworkHelper;

use AfricasTalking\SDK\AfricasTalking;

// use App\Traits\{
//     OtherFunctions
// };

/**
 * @group Sms helper class
 *
 * FUnctionalities for managing the sending of sms
 */

class SmsHelper implements EmptyHelperInterface
{
    public function __construct()
	{
    }
    /**
     * Send SMS
     */
	public function sendSms(string $message, string $recipient): array
	{   
        //$recipient = $this->getPhoneInInternationalFormat($recipient);
        
        return $this->africasTalking($message, $recipient);
    }

    /**
     * Send SMS
     * default provider: Ebulk sms
     */
	public function africasTalking($message, $recipient): array
	{    
        $providerUsername = env('AFRICAS_TALKING_USERNAME','');
        $providerKey = env('AFRICAS_TALKING_KEY','');


        // Initialize the SDK
        $AT  = new AfricasTalking($providerUsername, $providerKey);

        // Get the SMS service
        $sms = $AT->sms();
        
        try {
            // Thats it, hit send and we'll take care of the rest
            $result = $sms->send([
                'to'      => $recipient,
                'message' => $message
            ]);

            return $result;
        } catch (Exception $e) {
            Log::critical("Error: ".$e->getMessage());
        }
    }

}