<?php

namespace App\Helpers;

/**
 * @group User data management
 *
 * Core functions for for managing user data, retrieval, updates etc
 */

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Exceptions\HttpResponseException;

use Symfony\Component\HttpFoundation\Response;

use App\Repositories\{
    UserRepository
};

use App\Helpers\Contracts\EmptyHelperInterface;

use Carbon\Carbon;

use App\Jobs\{
    Sms, SendWelcomeMail
};

use App\Traits\{
   UserPermissions, OtherFunctions
};


class NotificationHelper implements EmptyHelperInterface
{
    use UserPermissions, OtherFunctions;

    public function __construct()
	{
	}

}