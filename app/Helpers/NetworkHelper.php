<?php

namespace App\Helpers;

//use Event;
use Carbon\Carbon;

use App\Helpers\Contracts\EmptyHelperInterface;


use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

//Exception 
use Exception;
use SoapClient;
use SoapFault;
use SoapHeader;
use SoapVar;


//http://docs.guzzlephp.org/en/stable/request-options.html#json
//https://stackoverflow.com/questions/22244738/how-can-i-use-guzzle-to-send-a-post-request-in-json
//GUZZLE HTTP 
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;


/**
 * @group Network calls
 *
 * APIs for making network calls
 */

class NetworkHelper implements EmptyHelperInterface
{
     //generic function for requests made using Guzzle
	public function makeGuzzleRequest($url, $data){//default null value for data
	    try {
	        //using the guzzlehettp api instead of the usual curl request

	        // Create a PSR-7 request object to send
	        $client = new Client([
            ]); //'headers' => [ "Content-Type"=>'application/json',]
	
			$response = $client->post($url,
                ['json' => $data ]); //header will be set to json automatically
			
            try {
	          	$statusCode = $response->getStatusCode();
			    if ($statusCode == 200) return $response->getBody()->getContents();

		    }catch(Exception $ex){
		     	var_dump(strval($ex->getMessage()) );
		    }

		    return false;
	    } catch (RequestException $ex) {
	        Log::error(__METHOD__.' Guzzle Request: '.$ex->getMessage());
	        if ($ex->hasResponse()) {
	        }//else //Log::error(__METHOD__.' Guzzle Request Error: '.Psr7\str($ex->getRequest()) );
	    }
	    return false;
	}

	public function makeRequestCurl($url, $authorization=null){
		//env('GOOGlE_API_KEY', 'nil');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		if(!empty($authorization)) curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization)); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        
        try {
	        $response = curl_exec($ch);
	        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	        if($response===FALSE){
	            return false;
	        }else {
	            curl_close($ch);
	            //return the result
	            if(!empty($response)) return array($status, $response);
	            else return false;
	        }
	    }catch(Exception $e){
	     	Log::error(__METHOD__.' Exception: '.$e->getMessage());
	    }
	    return false;
	}

	public function makeRequestCurlWithData(string $url, string $authorization=null, array $data){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		if(!empty($authorization)) curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization)); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        
        try {
	        $response = curl_exec($ch);
	        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	        if($response===FALSE){
	            return false;
	        }else {
	            curl_close($ch);
	            //return the result
	            if(!empty($response)) return array($status, $response);
	            else return false;
	        }
	    }catch(Exception $e){
	     	Log::error(__METHOD__.' Exception: '.$e->getMessage());
	    }
	    return false;
    }

}
