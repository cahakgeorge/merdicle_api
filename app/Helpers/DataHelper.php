<?php

namespace App\Helpers;

/**
 * @group User data management
 *
 * Core functions for for managing user data, retrieval, updates etc
 */

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Exceptions\HttpResponseException;


use App\Repositories\{
    UserRepository, DataRepository
};

use App\Helpers\Contracts\DataHelperInterface;

use Response;
use File;
use Carbon\Carbon;

use App\Traits\{
   UserPermissions, OtherFunctions
};

class DataHelper implements DataHelperInterface
{
    use UserPermissions, OtherFunctions;

    public function __construct()
	{
	}

    /** 
     * Get onboard data
     * @return array
     */ 
    public function getOnboardData(object $dataRepository): array
	{   
        $user = Auth::user(); //authenticated user

        $data = $dataRepository->getOnboardData();

        if(!$data) return ['status'=>false,'data'=>null,'message'=>'No content'];
        
        return ['status'=>true,'data'=>$data,'message'=>''];
    }

    //  ??????????????????????????????????????? POST LOGIN ???????????????????????????????????????

    /**
     * Create association
     * @authenticated
     * @return object
     */
    public function createAssociation(object $request, object $dataRepository): array
	{   
        $user = Auth::user(); //authenticated user

        //save profile image if exists
        $profilePic = $this->saveProfilePhoto($request);

        $associationArray = ['name'=>$request->name, 'admins'=>$request->admins, 'bio'=>$request->bio, 'pic_url'=>$profilePic, 'free_entry'=>$request->free_entry, 'created_at'=>Carbon::now(), 'created_by'=>$user->id];
       
        //create association
        $accountCreated = $dataRepository->createAssociation($associationArray);
        
        if(!$accountCreated) return ['status'=>false,'data'=>null, 'message'=>'association creation failed'];
        
        return ["status"=>true,'data'=> ['association'=>$accountCreated], 'message'=>''];
    }

    public function getAssociations(object $dataRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $associations = $dataRepository->getAssociations($user->id);

        if(!$associations) return ['status'=>false,'data'=>null,'message'=>'No content'];
        
        return ['status'=>true,'data'=>['associations'=>$associations],'message'=>''];
    }

    /**
     * Get association posts
     * @return array
     */
    public function getAssociationPosts(object $request, object $dataRepository): array
	{   
        $user = Auth::user(); //authenticated user
        $posts = $dataRepository->getAssociationPosts($request->assoc_id);

        if(!$posts) return ['status'=>false,'data'=>null,'message'=>'No content'];
        
        return ['status'=>true,'data'=>['posts'=>$posts],'message'=>''];
    }

    /**
     * Get association members
     * @return array
     */
    public function getAssociationMembers(object $request, object $dataRepository): array
	{   
        $user = Auth::user(); //authenticated user
        $members = $dataRepository->getAssociationMembers($request->assoc_id);

        if(!$members) return ['status'=>false,'data'=>null,'message'=>'No content'];
        
        return ['status'=>true,'data'=>['members'=>$members],'message'=>''];
    }

    /**
     * Create hub
     * @authenticated
     * @return object
     */
    public function createHub(object $request, object $dataRepository): array
	{   
        $user = Auth::user(); //authenticated user

        //save profile image if exists
        $profilePic = $this->saveProfilePhoto($request);

        $hubArray = ['name'=>$request->name, 'pic_url'=>$profilePic, 'created_at'=>Carbon::now(), 'creator_id'=>$user->id];
        
        //create hub
        $accountCreated = $dataRepository->createHub($hubArray);
        
        if(!$accountCreated) return ['status'=>false,'data'=>null, 'message'=>'hub creation failed'];
        
        return ["status"=>true,'data'=> ['hub'=>$accountCreated], 'message'=>''];
    }

    public function getHubs(object $dataRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $hubs = $dataRepository->getHubs($user->id);

        if($hubs->isEmpty()) return ['status'=>false,'data'=>null,'message'=>'No content'];
        
        return ['status'=>true,'data'=>['hubs'=>$hubs],'message'=>''];
    }

    /**
     * Get hub posts
     * @return array
     */
    public function getHubPosts(object $request, object $dataRepository): array
	{   
        $user = Auth::user(); //authenticated user
        $posts = $dataRepository->getHubPosts($request->hub_id);

        if(!$posts) return ['status'=>false,'data'=>null,'message'=>'No content'];
        
        return ['status'=>true,'data'=>['posts'=>$posts],'message'=>''];
    }

    /**
     * Get hub members
     * @return array
     */
    public function getHubMembers(object $request, object $dataRepository): array
	{   
        $user = Auth::user(); //authenticated user
        $members = $dataRepository->getHubMembers($request->hub_id);

        if(!$members) return ['status'=>false,'data'=>null,'message'=>'No content'];
        
        return ['status'=>true,'data'=>['members'=>$members],'message'=>''];
    }


    public function createHospital(object $request, object $dataRepository): object
	{      
        $user = Auth::user(); //authenticated user
        
        $hospitalDataArray = ['name'=>$request->name, 'owner'=>$user->id,
                                'address'=>$request->address, 'email'=>$request->email ?? '',
                                'phone'=>$request->phone ?? '', 'bio'=>$request->bio ?? '', 'latlng'=>$request->latlng ?? '',
                                'type'=>$request->type ?? '', 'recieve_appoints'=>$request->recieve_appoint ?? '', 'created_at'=>Carbon::now()];

        $createHospital = $dataRepository->createHospital($hospitalDataArray);
        if(!$createHospital) throw new HttpResponseException(response()->json(["status"=>false,"data"=>null,"message"=>"Failed to create hospital"], 204));

        return $createHospital;

        if(!$createHospital) return ['status'=>false,'data'=>null,'message'=>'Failed to create hospital'];
        
        return ['status'=>true,'data'=>['hospital'=>$createHospital],'message'=>''];
    }

    /**
     * Get hospitals
     * @return array
     */
    public function getHospitals(object $dataRepository): array
	{   
        $user = Auth::user(); //authenticated user
        $hospitals = $dataRepository->getHospitals();

        if(!$hospitals) return ['status'=>false,'data'=>null,'message'=>'No content'];

        return ['status'=>true,'data'=>['hospitals'=>$hospitals], 'message'=>''];
    }

    /**
     * Get hospitals nearby
     * @return array
     */
    public function getHospitalsNearby(object $request, object $dataRepository): array
	{   
        $user = Auth::user(); //authenticated user
        $userLocationArray = explode(',', $request->latlng);

        $hospitals = $dataRepository->getHospitals($request->hid);

        if(!$hospitals) throw new HttpResponseException(response()->json(["status"=>false,"data"=>null,"message"=>"No content"], 204));

        for ($i=0; $i<sizeof($hospitals); $i++){
            $hospitalI = $hospitals[$i];
            if(empty($hospitalI->latlng)){
                unset($hospitals[$i]);//if hospital doesnt have location attached, unset row, then skip in loop
                continue; 
            }

            $hospitalLocationArray = explode(',', $hospitalI->latlng);    

            $distanceInKm = ($this->vincentyGreatCircleDistance($userLocationArray[0], $userLocationArray[1], $hospitalLocationArray[0], $hospitalLocationArray[1]))/1000;
            $timeToTraverseInMin = round(($distanceInKm/35) * 60); //60 mins per hour //assume the driver goes at 60km/hr e.g distance of 40km at 80km/hr will take 0.5hours == 30mins //to get minutes (i.e 0.5 * 60mins/hr)
            
            $hospitals[$i]['time'] = $timeToTraverseInMin;
        }

        if(empty($hospitals)) return ['status'=>false,'data'=>null,'message'=>'No content'];

        return ['status'=>true,'data'=>['hospitals'=>$hospitals], 'message'=>''];
    }

    /**
     * Get hospitals via address
     * @return array
     */
    public function getHospitalsViaAddress(object $request, object $dataRepository): array
	{   
        $user = Auth::user(); //authenticated user
        
        $address = preg_replace("/[^A-Za-z,. ]/", "", $request->address); //strip all non-alphabetic characters from array
        //remove all single characters in string     https://stackoverflow.com/questions/7237083/php-function-to-remove-replace-all-single-characters-in-a-string
        $address = trim(preg_replace('@(^|\pZ)\pL\pM*(?=\pZ|$)@u', ' ', $address));

        $hospitalArray = []; //will hold the final array of found hospitals
        
        $keywords = $this->getKeywords($address);
        $filteredKeyword = $this->filterAddress($keywords);

        if(sizeof($filteredKeyword)<1) return response()->json(['code'=>'1', 'status'=>'Failed to retrieve hospitals'], 200);

        $hospitals = $dataRepository->getHospitalsViaAddress($filteredKeyword);
   
        if(!$hospitals) return ['status'=>false,'data'=>null,'message'=>'No content'];

        return ['status'=>true,'data'=>['hospitals'=>$hospitals], 'message'=>''];
    }

    public function getKeywords($address){
        $outputArray = array_filter(explode(' ', $address)); 
        return array_values($outputArray);
    }

    public function filterAddress($explodedAddress){
        $unneededKeyword = ['a','at','the','off','state','country']; //strip off popular keywords in most address entries
        for($i=0; $i<sizeof($explodedAddress); $i++){
            $explodedAddress[$i] = str_replace("street","", $explodedAddress[$i]);
            if(in_array($explodedAddress[$i], $unneededKeyword) ) unset($explodedAddress[$i]);
        }
        return $explodedAddress;
    }

    /**
     * Get hospital reviews
     * @return array
     */
    public function getHospitalReview(object $request, object $dataRepository): array
	{   
        $user = Auth::user(); //authenticated user
        $posts = $dataRepository->getHospitalReview($request->hid);

        if(!$posts) return ['status'=>false,'data'=>null,'message'=>'No content'];

        return ['status'=>true,'data'=>['reviews'=>$posts], 'message'=>''];
    }

    /**
     * Get profile picture
     * Request made by app
     * @authenticated
     * @return object
     */
    public function getProfilePic(string $imageName)
	{   
        if (filter_var($imageName, FILTER_VALIDATE_URL)) 
        {
            $response = $imageName;
        }else {
            $path = storage_path('app/public/profile_pics/' . $imageName);
            
            $file = File::get($path);
            $type = File::mimeType($path);
    
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
        }
        
        if(empty($response)) return ['status'=>false, 'msg'=>"Profile photo not found"];
        else {
            if (filter_var($response, FILTER_VALIDATE_URL)) return (strtolower(request()->header('PhoneType')) == "ios" ) ? ['status'=>true, 'data'=>$response] : ['status'=>true, 'photo'=>$response];
            else
            {
                throw new HttpResponseException($response);
                return $response;
            }
        }
    }

    /**
     * Get vid or picture file for post 
     * Request made by app
     * @authenticated
     * @return object
     */
    public function getVidPicFile(string $linkName)
	{   
        if (filter_var($linkName, FILTER_VALIDATE_URL)) 
        {
            $response = $linkName;
        }else {
            $path = storage_path('app/public/post_files/' . $linkName);
            
            $file = File::get($path);
            $type = File::mimeType($path);
    
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
        }
        
        if(empty($response)) return ['status'=>false, 'msg'=>"Ffile not found"];
        else {
            if (filter_var($response, FILTER_VALIDATE_URL)) return (strtolower(request()->header('PhoneType')) == "ios" ) ? ['status'=>true, 'data'=>$response] : ['status'=>true, 'photo'=>$response];
            else
            {
                throw new HttpResponseException($response);
                return $response;
            }
        }
    }

    /**
     * Get profile picture thumbnail
     * Request made by app
     * @authenticated
     * @return object
     */
    public function getProfilePicThumb(string $imageName)
	{   
        if (filter_var($imageName, FILTER_VALIDATE_URL)) 
        {
            $response = $imageName;
        }else {
            $path = storage_path('/app/public/profile_pics/thumbnails/'.$imageName);

            $file = File::get($path);
            $type = File::mimeType($path);

            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
        }
        
        if(empty($response)) return ['status'=>false, 'msg'=>"Profile photo not found"];
        else {
            if (filter_var($response, FILTER_VALIDATE_URL)) return (strtolower(request()->header('PhoneType')) == "ios" ) ? ['status'=>true, 'data'=>$response] : ['status'=>true, 'photo'=>$response];
            else
            {
                throw new HttpResponseException($response);
                return $response;
            }
        }
    }
}