<?php

namespace App\Helpers;

/**
 * @group User management
 *
 * APIs for managing users
 */

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

//use Event;
use Carbon\Carbon;

use App\Helpers\Contracts\AuthHelperInterface;

use App\Repositories\{
                        UserRepository
                    };

use Event;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

use App\Traits\{
    AuthFunctions, UserPermissions
};

class AuthHelper implements AuthHelperInterface
{
    use AuthFunctions, UserPermissions;
    
    protected $userRepository;

    public function __construct()
	{
	}
    
    /**
     * Authenticate user on platform
     * @bodyParam email string required The users email
     * @bodyParam password string required The users password
     */
    public function authenticateUser($request, $userRepository): array
	{   
        $firstTime =  false;
        if(!Auth::guard('web')->attempt($request->only('email', 'password') )) 
            return ["status"=>false,"data"=>null,"message"=>"Email or password incorrect"];
        
        $user = Auth::guard('web')->user();

        if(strtolower($user->status) != 'active')
            return ["status"=>false,"data"=>null,"message"=>"account ".$user->status];

        $userAccessToken = $userRepository->getAccessTokenForUser($user);
        
        $userCategory = $user->getRoleNames()->toArray()[0];
        if($userCategory == 'patient' && empty($user->genotype) && empty($user->bloodgroup) ) $firstTime = true;
        elseif($userCategory == 'doctor' && empty($user->folio_num) && empty($user->career_stage) && empty($user->specialty) ) $firstTime = true;

        return ['status'=>true,'data'=>['token'=> $userAccessToken, 'user_type'=>$user->getRoleNames()->toArray()[0], 'first_name'=>$user->first_name, 'last_name'=>$user->last_name,
                 'first_time'=>$firstTime, 'user_pic'=>$user->user_pic_url, 'specialty'=>$user->specialty],
                'message'=>''];                    
    }   

    
    /**
     * Logout user from platform
     */
	public function logoutUser(): array
    {   //validated data present
        $status = Auth::user()->token()->revoke();
        return ["status"=>$status, "data"=>null,"message"=>(!empty($status)) ? '' : 'logout failed'];
    }

}