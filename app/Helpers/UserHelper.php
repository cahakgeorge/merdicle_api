<?php

namespace App\Helpers;

/**
 * @group User data management
 *
 * Core functions for for managing user data, retrieval, updates etc
 */

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Exceptions\HttpResponseException;

use Illuminate\Support\Facades\Log;

use Symfony\Component\HttpFoundation\Response;

use App\Repositories\{
    UserRepository
};

use App\Helpers\Contracts\UserHelperInterface;

use Carbon\Carbon;

use App\Jobs\{
    Sms, SendWelcomeMail, SendVerifyMail
};

use App\Traits\{
   UserPermissions, OtherFunctions
};

use PaystackHelper;


class UserHelper implements UserHelperInterface
{
    use UserPermissions, OtherFunctions;

    public function __construct()
	{
	}

    
    public function signupUser(object $request, object $userRepository): array
	{
        $token = $this->generateToken();

        //check in db
        $tokenRecord = $userRepository->createEmailToken($token, $request->email);
        
        if(!$tokenRecord) return ['status'=>false,'data'=>null,'message'=>'failed to create account'];
        
        //send verify email to user
        //dispatch(new SendVerifyMail($request->first_name, $request->email, $token))
                //->delay(Carbon::now()->addSeconds(3));

        logger($token);
        
        Log::critical($this->getTokenMessage($token));

        return ['status'=>true,'data'=>null, 'message'=>''];
    }


    /**
     * Verify email token
     * @authenticated
     * @return object
     */
    public function verifyEmailToken(object $request, object $userRepository): array
	{   
        //save profile image if exists
        $profilePic = $this->saveProfilePhoto($request);

        //check in db
        $accountCreated = $userRepository->verifyEmailTokenThenCreateAccount($request, $profilePic);
        
        if(!$accountCreated) return ['status'=>false,'data'=>null,'message'=>'user not registered'];
        
        //mail
        //dispatch(new SendWelcomeMail($accountCreated->first_name, $accountCreated->email));
        Log::critical("User $accountCreated->first_name signed up Merdicle");

        $userAccessToken = $userRepository->getAccessTokenForUser($accountCreated);

        return ['status'=>true,'data'=>['first_name'=>$accountCreated->first_name, 'last_name'=>$accountCreated->last_name, 
                 'user_type'=>$accountCreated->user_type, 'token'=>$userAccessToken, 'first_time'=>true, 'user_pic'=>$accountCreated->user_pic_url], 'message'=>''];   

    }

    

    //LEVEL 2

    /**
     * Forgot password
     * @authenticated
     * @return object
     */
    public function initiateForgotPassword(object $request, object $userRepository): array
	{
        $createdToken = $this->generateToken();

        //search and persist gen token to db
        $dbResult = $userRepository->createForgotPasswordToken($request->email, $createdToken);
        
        if(!$dbResult[0])  return ['status'=>false,'data'=>null,'message'=>'user does not exist'];
        
        //send reset mail
        //dispatch(new ResetAccountMail($token, $request->email));
        
        logger('Forgot password '.$createdToken);
        Log::critical('Forgot password '.$this->getTokenMessage($createdToken));

        return ['status'=>true,'data'=>null, 'message'=>''];
    }

    /**
     * Verify forgot password token
     * @authenticated
     * @return object
     */
    public function updatePasswordViaForgotToken(object $request, object $userRepository): array
	{   
        //check in db
        $resetResult = $userRepository->verifyForgotPasswordTokenThenUpdatePassword($request);

        if(!$resetResult) return ["status"=>false,"data"=>null,"message"=>"No such user found"];
        
        return ['status'=>true,'data'=>null, 'message'=>''];
    }

    /**
     * Update patient bio
     * @authenticated
     * @return object
     */
    public function updatePatientBio(object $request, object $userRepository): array
	{   
        $user = Auth::user(); //authenticated user
        $update = $userRepository->updatePatientBio($user, $request);

        if(!$update)  return ["status"=>false,"data"=>null,"message"=>"No such user found"];
        
        return ['status'=>true,'data'=>null, 'message'=>''];
    }

    /**
     * Update doctor bio
     * @authenticated
     * @return object
     */
    public function updateDoctorBio(object $request, object $userRepository): array
	{
        $user = Auth::user(); //authenticated user

        $update = $userRepository->updateDoctorBio($user, $request);

        if(!$update) return ["status"=>false,"data"=>null,"message"=>"No such user found"];
        
        return ['status'=>true,'data'=>null, 'message'=>''];
    }



    //  ??????????????????????????????????????? AFTER LOGIN ???????????????????????????????????????
    //  ??????????????????????????????????????? AFTER LOGIN ???????????????????????????????????????
    //  ??????????????????????????????????????? AFTER LOGIN ???????????????????????????????????????
    //  ??????????????????????????????????????? AFTER LOGIN ???????????????????????????????????????
    //  ??????????????????????????????????????? AFTER LOGIN ???????????????????????????????????????

    /**
     * Get overview of users account
     * @return array
     */
    /* public function getAccountOverview(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        $overview = $userRepository->getAccountOverview($user->id);

        if(!$overview) return ["status"=>false,"data"=>null,"message"=>"No content"];
        
        return ['status'=>true,'data'=>['overview'=>$overview], 'message'=>''];
    } */

    /**
     * Update user password
     * @return bool
     */
    public function updatePasswordFromProfile(object $request, object $userRepository): array
	{
        $user = Auth::user(); //authenticated user

        $updateResult = $userRepository->updatePasswordFromProfile($user, $request);

        if(!$updateResult) return ["status"=>false,"data"=>null,"message"=>"No content"];
        
        return ['status'=>true,'data'=>null, 'message'=>''];
    }

    /**
     * Get user profile
     * @return array
     */
    public function getUserProfile(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        $userProfile = $userRepository->getUserProfile($request->user_id, $myUserId=$user->id);

        if(!$userProfile) return ["status"=>false,"data"=>null,"message"=>"No content"];
        
        return ['status'=>true,'data'=>['profile'=>$userProfile], 'message'=>''];
    }

    /**
     * Get my profile
     * @return array
     */
    public function getMyProfile(object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        $userProfile = $userRepository->getUserProfile($user->id, $user->id);

        if(!$userProfile) return ["status"=>false,"data"=>null,"message"=>"No content"];
        
        return ['status'=>true,'data'=>$userProfile, 'message'=>''];
    }

    /**
     * Update user profile
     * @return array
     */
    public function updateUserProfile(object $request, object $userRepository): array
	{   
        $user = Auth::user(); //authenticated user

        $updateResult = $userRepository->updateUserProfile($user, $request);

        if(!$updateResult) return ["status"=>false,"data"=>null,"message"=>"Update failed"];
        
        return ['status'=>true,'data'=>null, 'message'=>''];
    }


    /**
     * Delete user account
     * Request made by app
     * @authenticated
     * @return object
     */
    public function deleteUserAccount(object $userRepository): array
	{   
        $user = Auth::user(); //authenticated user
        
        Log::critical("$user->first_name $user->first_name with email: $user->email account was deleted");
        $userRepository->deleteAccount($user);

        return ["status"=>true,"data"=>null,"message"=>"Successfully deleted account"];
    }


    /**
     * Get posts for user
     * @return array
     */
    public function getPosts(object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        $posts = $userRepository->getPosts($user->id);

        if(!$posts) return ["status"=>false,"data"=>null,"message"=>"No content"];
        
        return ['status'=>true,'data'=>$posts, 'message'=>''];
    }

    /**
     * Get all posts for single user
     * @return array
     */
    public function getAllPostsForUser($request, object $userRepository): array
	{      
        //$user = Auth::user(); //authenticated user
        $posts = $userRepository->getAllPostsForUser($request->user_id);

        if(!$posts) return ["status"=>false,"data"=>null,"message"=>"No content"];
        
        return ['status'=>true,'data'=>['posts'=>$posts], 'message'=>''];
    }

    /**
     * Get all posts made by a specific user
     * @return array
     */
    public function getSpecificUserPost(object $request, object $userRepository): array
	{      
        //$user = Auth::user(); //authenticated user
        $posts = $userRepository->getSpecificUserPost($request->user_id);

        if(!$posts) return ["status"=>false,"data"=>null,"message"=>"No content"];
        
        return ['status'=>true,'data'=>['posts'=>$posts], 'message'=>''];
    }

    public function createPost(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user

        //get attached doc
        if($request->has('img')) $type = 'img';
        else if($request->has('vid')) $type = 'vid';
        else if($request->has('doc')) $type = 'doc';
        
        if(isset($type)) $fileStorageName = $this->saveOtherFiles($user->id, $request, $type, 'post_files');
        
        $postDataArray = ['user_id'=>$user->id, 'post_type'=>$request->post_type, 'visibility'=>$request->post_visib,
                            'post_is_for'=>$request->post_is_for ?? '', 'owner_id'=>$request->post_owner_id,
                            'content'=>$request->post_text ?? '', 'has_extra'=>$type ?? 'nil', 'extra_url'=>$fileStorageName ?? '', 
                            'created_at'=>Carbon::now()];

        $createPost = $userRepository->createPost($postDataArray);

        if(!$createPost) return ["status"=>false,"data"=>null,"message"=>"Failed to create post"];
        
        return ['status'=>true,'data'=>null, 'message'=>''];
    }

    public function likePost(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $upate = $userRepository->likePost($user->id, $request->post_id);

        return ['status'=>!empty($upate),'data'=>null, 'message'=>''];
    }

    public function unlikePost(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $upate = $userRepository->unlikePost($user->id, $request->post_id);

        return ['status'=>!empty($upate),'data'=>null, 'message'=>''];
    }

    /**
     * Get post comment
     * @return array
     */
    public function getPostComment(object $request, object $userRepository): array
	{     
        $user = Auth::user(); //authenticated user
        $postComment = $userRepository->getPostComment($request->post_id);
        
        if(!$postComment) return ["status"=>false,"data"=>null,"message"=>"No content"];
        
        return ['status'=>true,'data'=>['post_comment'=>$postComment], 'message'=>''];
    }   

    public function createPostComment(object $request, object $userRepository): array
	{   
        $user = Auth::user(); //authenticated user

        //get attached doc
        if($request->has('image')) $type = 'image';
        else if($request->has('video')) $type = 'video';
        else if($request->has('doc')) $type = 'doc';
        
        if(isset($type)) $fileStorageName = $this->saveOtherFiles($user->id, $request, $type, 'post_files');
        
        $postDataArray = ['user_id'=>$user->id, 'post_type'=>'normal',
                            'post_is_for'=>'comment', 'owner_id'=>$request->post_id,
                            'content'=>$request->post_text ?? '', 'has_extra'=>$type ?? 'nil', 'extra_url'=>$fileStorageName ?? '',
                            'created_at'=>Carbon::now()];

        $createPost = $userRepository->createPost($postDataArray);

        if(!$createPost) return ["status"=>false,"data"=>null,"message"=>"Failed to add comment"];
        
        return ['status'=>true,'data'=>null, 'message'=>''];
    }


    public function deletePostComment(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $upate = $userRepository->deletePostComment($user->id, $request->post_id);

        return ['status'=>!empty($upate),'data'=>null, 'message'=>''];
    }

    public function getBookmarkedPosts(object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $userBookmarks = $userRepository->getBookmarkedPosts($user->id);

        if(!$userBookmarks) return ["status"=>false,"data"=>null,"message"=>"No content"];
        
        return ['status'=>true,'data'=>['user_bookmarks'=>$userBookmarks], 'message'=>''];
    }

    public function createPostBookmark(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $upate = $userRepository->createPostBookmark($user->id, $request->post_id);

        return ['status'=>!empty($upate),'data'=>null, 'message'=>''];
    }

    public function deletePostBookmark(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $upate = $userRepository->deletePostBookmark($user->id, $request->post_id);

        return ['status'=>!empty($upate),'data'=>null, 'message'=>''];
    }

    public function getUsersWhoSharedPost(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $users = $userRepository->getUsersWhoSharedPost($request->post_id);

        return ['status'=>!empty($users),'data'=>['users'=>$users], 'message'=>!empty($users) ? '' : 'No content'];
    }
    
    public function sharePost(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        $postDataArray = ['user_id'=>$user->id, 'post_type'=>'normal',
                            'post_is_for'=>'share', 'owner_id'=>$request->post_id,
                            'content'=>$request->post_text ?? '', 'has_extra'=>'nil', 'created_at'=>Carbon::now()];

        $createPostShare = $userRepository->createPost($postDataArray);

        if(!$createPostShare) return ["status"=>false,"data"=>null,"message"=>"No content"];
        
        return ['status'=>true,'data'=>null, 'message'=>''];
    }

    public function getUserNotifications(object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $userNotifications = $userRepository->getUserNotifications($user->id);

        if(!$userNotifications) return ["status"=>false,"data"=>null,"message"=>"No content"];
        
        return ['status'=>true,'data'=>['notifications'=>$userNotifications], 'message'=>''];
    }

    public function followUser(object $request, object $userRepository): array
	{   
        $user = Auth::user(); //authenticated user
        
        $response = $userRepository->followUser($user->id, $userToFollow = $request->user_id);

        return ['status'=>!empty($response),'data'=>null, 'message'=>!empty($response) ? '' : 'Failed to follow user'];
    }

    public function unfollowUser(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $response = $userRepository->unfollowUser($user->id, $userToFollow = $request->user_id);

        return ['status'=>!empty($response),'data'=>null, 'message'=>!empty($response) ? '' : 'Failed to unfollow user'];
    }

    public function blockUser(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $response = $userRepository->blockUser($user->id, $userToBlock = $request->user_id);
        
        return ['status'=>!empty($response),'data'=>null, 'message'=>!empty($response) ? '' : 'Failed to block user'];
    }

    public function getCases(object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $cases = $userRepository->getCases();

        if(!$cases) return ["status"=>false,"data"=>null,"message"=>"No content"];
        
        return ['status'=>true,'data'=>['cases'=>$cases], 'message'=>''];
    }

    public function createCase(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user

        if($request->has('image')) $type = 'image';
        if(isset($type)) $fileStorageName = $this->saveOtherFiles($user->id, $request, $type, 'post_files');

        $caseDataArray = ['creator_id'=>$user->id, 'title'=>$request->title,
                            'case_description'=>$request->title, 'symptoms'=>$request->symptoms,
                            'diagnosis'=>$request->diagnosis, 'complications'=>$request->complications, 
                            'causes'=>$request->causes, 'treatment'=>$request->treatment,
                            'has_extra'=>$type ?? 'nil', 'extra_url'=>$fileStorageName ?? '',
                            'created_at'=>Carbon::now()];

        $createCase = $userRepository->createCase($caseDataArray);
        if(!$createCase) return ["status"=>false,"data"=>null,"message"=>"Failed to create case"];
        
        return ['status'=>true,'data'=>null, 'message'=>''];
    }
    
    /**
     * Get case reviews
     * @return array
     */
    public function getCaseReviews(object $request, object $userRepository): array
	{     
        $user = Auth::user(); //authenticated user
        $reviews = $userRepository->getCaseReviews($request->case_id);
        
        if(!$reviews) return ["status"=>false,"data"=>null,"message"=>"No content"];
        
        return ['status'=>true,'data'=>['reviews'=>$reviews], 'message'=>''];
    } 

    public function createCaseReview(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $reviewDataArray = ['user_id'=>$user->id, 'post_type'=>'normal',
                            'post_is_for'=>'case_review', 'owner_id'=>$request->case_id,
                            'content'=>$request->review_text, 'has_extra'=>'nil', 'created_at'=>Carbon::now()];

        $createReview = $userRepository->createPost($reviewDataArray);
        if(!$createReview) return ["status"=>false,"data"=>null,"message"=>"Failed to create review"];
        
        return ['status'=>true,'data'=>null, 'message'=>''];
    }

    public function createCaseRating(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user

        $caseRatingDataArray = ['user_id'=>$user->id, 'case_id'=>$request->case_id,
                            'rating'=>$request->rating, 'created_at'=>Carbon::now()];

        $dbResult = $userRepository->createCaseRating($caseRatingDataArray);
        if(!$dbResult) return ["status"=>false,"data"=>null,"message"=>"Failed to rate case"];
        
        return ['status'=>true, 'data'=>null, 'message'=>''];
    }


    /**
     * Create hospital review
     * @return array
     */
    public function createHospitalReview(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $reviewDataArray = ['user_id'=>$user->id, 'post_type'=>'normal',
                            'post_is_for'=>'hosp_review', 'owner_id'=>$request->hid,
                            'content'=>$request->review_text, 'has_extra'=>'nil', 'created_at'=>Carbon::now()];

        $createReview = $userRepository->createPost($reviewDataArray);
        if(!$createReview) return ["status"=>false,"data"=>null,"message"=>"Failed to create review"];
        
        return ['status'=>true,'data'=>null, 'message'=>''];
    }

    public function getConnectionScreenData(object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user

        $dbResult = $userRepository->getMyConnections($user->id); //an array is returned here
        if(empty($dbResult)) return ["status"=>false,"data"=>null,"message"=>"No content"];
        
        return ['status'=>true, 'data'=>$dbResult, 'message'=>''];
    }

    public function joinAssociation(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $response = $userRepository->joinAssociation($user->id, $request->assoc_id);

        return ['status'=>!empty($response),'data'=>null, 'message'=>!empty($response) ? '' : 'Failed to join association'];
    }

    public function leaveAssociation(object $request, object $userRepository): array
	{   
        $user = Auth::user(); //authenticated user
        
        $response = $userRepository->leaveAssociation($user->id, $request->assoc_id);

        return ['status'=>!empty($response),'data'=>null, 'message'=>!empty($response) ? '' : 'Failed to leave association'];
    }

    public function joinHub(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $response = $userRepository->joinHub($user->id, $request->hub_id);

        return ['status'=>!empty($response),'data'=>null, 'message'=>!empty($response) ? '' : 'Failed to join hub'];

    }

    public function leaveHub(object $request, object $userRepository): array
	{   
        $user = Auth::user(); //authenticated user
        
        $response = $userRepository->leaveHub($user->id, $request->hub_id);

        return ['status'=>!empty($response),'data'=>null, 'message'=>!empty($response) ? '' : 'Failed to leave hub'];
    }



    public function createPatientAppointment(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $appointmentDataArray = ['user_id'=>$user->id, 'doctor_id'=>$request->did,
                                'title'=>$request->title,
                                'date'=>$request->date ?? '', 'location'=>$request->address ?? '', 'created_at'=>Carbon::now(),
                                'edit_log'=>"start|$user->id|patient|$request->address|$request->date"]; //start|patient|10 dahomey street|2019-09-2019 10:00:00

        $createAppointment = $userRepository->createPatientAppointment($appointmentDataArray);
        if(!$createAppointment) return ["status"=>false,"data"=>null,"message"=>"Failed to create appointment"];
        
        return ['status'=>true,'data'=>null, 'message'=>''];
    }


    /**
     * Get user appointments
     * @return array
     */ 
    public function getUserAppointments(object $userRepository): array
	{     
        $user = Auth::user(); //authenticated user
        $appointments = $userRepository->getUserAppointments($user->id);
        
        if(!$appointments) return ["status"=>false,"data"=>null,"message"=>"No content"];
        
        return ['status'=>true,'data'=>['appointments'=>$appointments], 'message'=>''];
    }   

    public function approveAppointment(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $dbUpdateResult = $userRepository->approveAppointment($user->id, $request->appoint_id);

        if(!$dbUpdateResult) return ["status"=>false,"data"=>null,"message"=>"Failed to approve appointment"];
        
        return ['status'=>true,'data'=>null, 'message'=>''];
    }
    
    public function editAppointment(object $request, object $userRepository): array
	{      
        $user = Auth::user(); //authenticated user
        
        $currentAppointment = $userRepository->getCurrentAppointment($user->id, $request->appoint_id);
        if(!$currentAppointment) throw new HttpResponseException(response()->json(["status"=>false,"data"=>null,"message"=>"Appointment data not found"], 204));

        $editLogUpdate = $currentAppointment->edit_log.'#*#'."start|$user->id|$user->user_type|$request->address|$request->date";

        $appointmentUpdataArray = ['date'=>$currentAppointment->date, 'location'=>$request->address ?? '', 'updated_at'=>Carbon::now(),
                                'edit_log'=>$editLogUpdate]; //start|patient|10 dahomey street|2019-09-2019 10:00:00


        $dbUpdateResult = $userRepository->editAppointment($currentAppointment, $appointmentUpdataArray);

        if(!$dbUpdateResult) return ["status"=>false,"data"=>null,"message"=>"Failed to edit appointment"];
        
        return ['status'=>true,'data'=>null, 'message'=>''];
    }

}