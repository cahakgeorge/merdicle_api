<?php

namespace App\Helpers\Contracts;


interface AuthHelperInterface
{
    public function authenticateUser($request, $userRepository): array;
    
    public function logoutUser(): array;
}